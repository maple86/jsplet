package com.maple86.jsplet.config;

import com.maple86.jsplet.db.TransactionLevel;
import com.maple86.jsplet.map.HashTypeMap;
import com.maple86.jsplet.map.TypeMap;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JspletSetting {

    /**
     * 文件上传最大内存限制
     */
    private int uploadMemoryThreshold = 3145728;

    /**
     * 文件上传单个文件最大限制
     */
    private long uploadFileMaxSize = 10485760;

    /**
     * 文件上传单个请求最大限制
     */
    private long uploadRequestMaxSize = 12582912;

    /**
     * 是否开启数据库字段自动将下划线分隔转成驼峰命名
     */
    private boolean mapUnderscoreToCamelCase = true;

    /**
     * 默认的事物等级
     */
    private TransactionLevel defaultTransactionLevel = TransactionLevel.TRANSACTION_REPEATABLE_READ;

    /**
     * 是否打印异常
     */
    private boolean printException;

    /**
     * action请求的后缀
     */
    private String actionSuffix = ".do";

    /**
     * 视图页面的后缀
     */
    private String viewSuffix = ".jsp";

    /**
     * 是否检测页面修改
     */
    private boolean detectPageChange = false;

    public void load(Element element) {
        NodeList nodeList = element.getElementsByTagName("setting");
        int length = nodeList.getLength();
        TypeMap map = new HashTypeMap();
        for(int i = 0; i < length; i++) {
            Node node = nodeList.item(i);
            if (!(node instanceof Element)) {
                continue;
            }
            Element ele = (Element) node;
            String name = ele.getAttribute("name");
            String value = getValue(ele);
            if (null == value) {
                continue;
            }
            map.put(name, value);
        }
        if (map.containsKey("uploadMemoryThreshold")) {
            uploadMemoryThreshold = (int) parseSize(map.getString("uploadMemoryThreshold"));
        }
        if (map.containsKey("uploadFileMaxSize")) {
            uploadFileMaxSize = parseSize(map.getString("uploadFileMaxSize"));
        }
        if (map.containsKey("uploadRequestMaxSize")) {
            uploadRequestMaxSize = parseSize(map.getString("uploadRequestMaxSize"));
        }
        if (map.containsKey("mapUnderscoreToCamelCase")) {
            mapUnderscoreToCamelCase = map.getBoolean("mapUnderscoreToCamelCase");
        }
        if (map.containsKey("defaultTransactionLevel")) {
            defaultTransactionLevel = map.get("defaultTransactionLevel", TransactionLevel.class);
        }
        if (map.containsKey("actionSuffix")) {
            actionSuffix = map.getString("actionSuffix");
        }
        if (map.containsKey("printException")) {
            printException = map.getBoolean("printException");
        }
        if (map.containsKey("viewSuffix")) {
            viewSuffix = map.getString("viewSuffix");
        }
        if (map.containsKey("detectPageChange")) {
            detectPageChange = map.getBoolean("detectPageChange");
        }
    }

    private String getValue(Element element) {
        if (element.hasAttribute("value")) {
            return element.getAttribute("value");
        }
        NodeList nodeList = element.getElementsByTagName("value");
        int length = nodeList.getLength();
        for(int i = 0; i < length; i++) {
            Node node = nodeList.item(i);
            if (!(node instanceof Element)) {
                continue;
            }
            Element ele = (Element) node;
            return ele.getTextContent();
        }
        return null;
    }

    private long parseSize(String value) {
        if (value.endsWith("K")) {
            String val = value.substring(0, value.length() - 1);
            return Long.parseLong(val) * 1024L;
        }
        if (value.endsWith("M")) {
            String val = value.substring(0, value.length() - 1);
            return Long.parseLong(val) * 1024L * 1024L;
        }
        if (value.endsWith("G")) {
            String val = value.substring(0, value.length() - 1);
            return Long.parseLong(val) * 1024L * 1024L * 1024L;
        }
        return Long.parseLong(value);
    }

    public int getUploadMemoryThreshold() {
        return uploadMemoryThreshold;
    }

    public long getUploadFileMaxSize() {
        return uploadFileMaxSize;
    }

    public long getUploadRequestMaxSize() {
        return uploadRequestMaxSize;
    }

    public boolean isMapUnderscoreToCamelCase() {
        return mapUnderscoreToCamelCase;
    }

    public TransactionLevel getDefaultTransactionLevel() {
        return defaultTransactionLevel;
    }

    public boolean isPrintException() {
        return printException;
    }

    public String getActionSuffix() {
        return actionSuffix;
    }

    public String getViewSuffix() {
        return viewSuffix;
    }

    public boolean isDetectPageChange() {
        return detectPageChange;
    }

}
