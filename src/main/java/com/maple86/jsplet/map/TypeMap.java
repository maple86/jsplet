package com.maple86.jsplet.map;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public interface TypeMap extends Map<String, Object> {

    void putAll(Object model);

    <T> T get(String key, Class<T> type);

    String getString(String key);

    Byte getByte(String key);

    Short getShort(String key);

    Integer getInteger(String key);

    Long getLong(String key);

    Double getDouble(String key);

    Boolean getBoolean(String key);

    Float getFloat(String key);

    Date getDate(String key);

    BigDecimal getBigDecimal(String key);

}
