package com.maple86.jsplet.map;

import com.maple86.jsplet.util.BeanUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class HashTypeMap extends LinkedHashMap<String, Object> implements TypeMap {

    public HashTypeMap() {
    }

    public HashTypeMap(Object bean) {
        putAll(bean);
    }

    @Override
    public void putAll(Object bean) {
        Map<String, Object> map = BeanUtil.parseMap(bean);
        this.putAll(map);
    }

    @Override
    public <T> T get(String key, Class<T> type) {
        Object value = get(key);
        if (null == value) {
            return null;
        }
        T result = BeanUtil.parseType(value, type);
        if (null == result) {
            throw new ClassCastException("类型转换错误！");
        }
        return result;
    }

    @Override
    public String getString(String key) {
        return get(key, String.class);
    }

    @Override
    public Byte getByte(String key) {
        return get(key, Byte.class);
    }

    @Override
    public Short getShort(String key) {
        return get(key, Short.class);
    }

    @Override
    public Integer getInteger(String key) {
        return get(key, Integer.class);
    }

    @Override
    public Long getLong(String key) {
        return get(key, Long.class);
    }

    @Override
    public Double getDouble(String key) {
        return get(key, Double.class);
    }

    @Override
    public Boolean getBoolean(String key) {
        return get(key, Boolean.class);
    }

    @Override
    public Float getFloat(String key) {
        return get(key, Float.class);
    }

    @Override
    public Date getDate(String key) {
        return get(key, Date.class);
    }

    @Override
    public BigDecimal getBigDecimal(String key) {
        return get(key, BigDecimal.class);
    }
}
