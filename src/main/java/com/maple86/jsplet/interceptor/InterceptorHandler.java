package com.maple86.jsplet.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface InterceptorHandler {

    Object handle(HttpServletRequest request, HttpServletResponse response, InterceptorInvocation invocation) throws Throwable;

}
