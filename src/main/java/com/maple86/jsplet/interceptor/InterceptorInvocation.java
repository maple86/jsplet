package com.maple86.jsplet.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InterceptorInvocation {

    private final InterceptorHandler handler;

    private final InterceptorInvocation invocation;

    public InterceptorInvocation(InterceptorHandler handler, InterceptorInvocation invocation) {
        this.handler = handler;
        this.invocation = invocation;
    }

    public Object invoke(HttpServletRequest request, HttpServletResponse response) throws Throwable {
        return handler.handle(request, response, invocation);
    }

}
