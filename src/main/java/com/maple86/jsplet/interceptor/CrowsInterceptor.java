package com.maple86.jsplet.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrowsInterceptor implements InterceptorHandler {

    private String allowOrigin = "*";

    private String allowMethods = "GET, POST, PUT, DELETE, OPTIONS";

    private String maxAge = "3600";

    private String allowHeaders = "x-requested-with";

    public void setAllowOrigin(String allowOrigin) {
        this.allowOrigin = allowOrigin;
    }

    public void setAllowMethods(String allowMethods) {
        this.allowMethods = allowMethods;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public void setAllowHeaders(String allowHeaders) {
        this.allowHeaders = allowHeaders;
    }

    @Override
    public Object handle(HttpServletRequest request, HttpServletResponse response, InterceptorInvocation invocation) throws Throwable {
        String method = request.getMethod();
        response.setHeader("Access-Control-Allow-Origin", allowOrigin);
        response.setHeader("Access-Control-Allow-Methods", allowMethods);
        response.setHeader("Access-Control-Max-Age", maxAge);
        response.setHeader("Access-Control-Allow-Headers", allowHeaders);
        if ("OPTIONS".equals(method)) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return null;
        }
        return invocation.invoke(request, response);
    }
}
