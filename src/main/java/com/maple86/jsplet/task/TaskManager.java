package com.maple86.jsplet.task;

import com.maple86.jsplet.annotation.TaskMethod;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletEngineAware;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.exception.JspletConfigException;
import com.maple86.jsplet.exception.JspletException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TaskManager implements JspletEngineAware {

    private final Log log = LogFactory.getLog(this.getClass());

    private JspletEngine jspletEngine;

    private ScheduledExecutorService scheduledExecutorService;

    private final List<TaskItem> taskItems = new LinkedList<>();

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @PostConstruct
    public void init() {
        try {
            List<String> moduleNames = jspletEngine.getModuleNames();
            for (String moduleName : moduleNames) {
                JspletModule jspletModule = jspletEngine.getModule(moduleName);
                List<JspletPage> jspletPages = jspletModule.getJspletPages("task");
                for (JspletPage jspletPage : jspletPages) {
                    registerTask(jspletPage);
                }
            }
        } catch (ParseException ex) {
            throw new JspletException(ex);
        }
        if (!taskItems.isEmpty()) {
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            schedule();
        }
    }

    @PreDestroy
    public void destroy() {
        if (null != scheduledExecutorService) {
            scheduledExecutorService.shutdownNow();
        }
    }

    private void registerTask(JspletPage jspletPage) throws ParseException {
        Object target = jspletPage.getTarget();
        Method[] methods = target.getClass().getMethods();
        for (Method method : methods) {
            TaskMethod taskMethod = method.getAnnotation(TaskMethod.class);
            if (null == taskMethod) {
                continue;
            }
            Parameter[] parameters = method.getParameters();
            if (parameters.length > 0) {
                throw new JspletConfigException("任务方法不能有参数！");
            }
            TaskItem taskItem = new TaskItem(jspletPage, method.getName());
            taskItem.init();
            joinTask(taskItem);
        }
    }

    private void joinTask(TaskItem taskItem) {
        if (null == taskItem.getTime()) {
            return;
        }
        int index = 0;
        for (TaskItem item : taskItems) {
            if (item.getTime().getTime() > taskItem.getTime().getTime()) {
                taskItems.add(index, taskItem);
                return;
            }
            index++;
        }
        taskItems.add(taskItem);
    }

    private void execute() {
        TaskItem taskItem = taskItems.remove(0);
        taskItem.next();
        joinTask(taskItem);
        try {
            taskItem.run();
        } catch (Exception ex) {
            log.error("", ex);
        }
        schedule();
    }

    private void schedule() {
        if (taskItems.isEmpty()) {
            return;
        }
        TaskItem taskItem = taskItems.get(0);
        long time = taskItem.getTime().getTime();
        long delay = time - System.currentTimeMillis();
        if (delay < 0L) {
            delay = 0L;
        }
        scheduledExecutorService.schedule(this::execute, delay, TimeUnit.MILLISECONDS);
    }

}
