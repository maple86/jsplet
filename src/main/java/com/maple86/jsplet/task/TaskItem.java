package com.maple86.jsplet.task;

import com.maple86.jsplet.annotation.TaskMethod;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.CloseSessionInvoker;
import com.maple86.jsplet.weaver.ComponentWeaverInvoker;
import com.maple86.jsplet.weaver.WeaverInvoker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Date;

public class TaskItem {

    private final Log log = LogFactory.getLog(getClass());

    private final JspletPage jspletPage;

    private final String methodName;

    private CronExpression cronExpression;

    private long interval;

    private int repeat;

    private int count;

    private Date time;

    public TaskItem(JspletPage jspletPage, String methodName) {
        this.jspletPage = jspletPage;
        this.methodName = methodName;
    }

    public Date getTime() {
        return time;
    }

    public void init() throws ParseException {
        Method method = getMethod();
        if (null == method) {
            return;
        }
        TaskMethod taskMethod = method.getAnnotation(TaskMethod.class);
        String cron = taskMethod.cron();
        if (null != cron && !"".equals(cron)) {
            cronExpression = new CronExpression(cron);
        }
        long delay = taskMethod.delay();
        interval = taskMethod.interval();
        repeat = taskMethod.repeat();
        if (delay < 0L) {
            delay = 0L;
        }
        if (interval < 1L) {
            interval = 1L;
        }
        if (null != cronExpression) {
            time = cronExpression.getTimeAfter(new Date());
        } else {
            time = new Date(System.currentTimeMillis() + delay);
        }
    }

    public void next() {
        if (null != cronExpression) {
            time = cronExpression.getTimeAfter(new Date());
        } else {
            if (repeat != -1) {
                count++;
                if (count >= repeat) {
                    time = null;
                    return;
                }
            }
            time = new Date(System.currentTimeMillis() + interval);
        }
    }

    public void run() {
        try {
            JspletEngine jspletEngine = jspletPage.getJspletModule().getJspletEngine();
            JspletSetting jspletSetting = jspletEngine.getJspletSetting();
            if (jspletSetting.isDetectPageChange()) {
                jspletPage.detectChange();
            }
            Object target = jspletPage.getTarget();
            if (null == target) {
                return;
            }
            Method method = getMethod();
            if (null == method) {
                return;
            }
            log.trace("开始执行，页面：" + jspletPage.getPagePath() + "，方法：" + method.getName());
            WeaverInvoker weaverInvoker = new ComponentWeaverInvoker(jspletPage, (tar, me, as) -> jspletPage.invoke(me, as));
            CloseSessionInvoker closeSessionInvoker = new CloseSessionInvoker(jspletEngine, weaverInvoker);
            Object result = closeSessionInvoker.invoke(target, method);
            log.trace("结束执行，页面：" + jspletPage.getPagePath() + "，方法：" + method.getName());
            if (log.isDebugEnabled()) {
                log.trace("执行结果：" + result);
            }
        } catch (Throwable ex) {
            log.error("", ex);
        }
    }

    private Method getMethod() {
        Object target = jspletPage.getTarget();
        if (null == target) {
            return null;
        }
        Class<?> type = target.getClass();
        try {
            return type.getMethod(methodName);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

}
