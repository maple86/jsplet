package com.maple86.jsplet.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class ForEachTagSupport extends SimpleTagSupport {

    private String collection;

    private String item;

    private String separator;

    private String open;

    private String close;

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public void setClose(String close) {
        this.close = close;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        getJspBody().invoke(writer);
        String text = writer.toString();
        JspWriter out = getJspContext().getOut();
        StringBuilder buf = new StringBuilder();
        buf.append("<foreach");
        if (null != collection && !"".equals(collection)) {
            buf.append(" collection = \"").append(collection).append("\"");
        }
        if (null != item && !"".equals(item)) {
            buf.append(" item = \"").append(item).append("\"");
        }
        if (null != separator && !"".equals(separator)) {
            buf.append(" separator = \"").append(separator).append("\"");
        }
        if (null != open && !"".equals(open)) {
            buf.append(" open = \"").append(open).append("\"");
        }
        if (null != close && !"".equals(close)) {
            buf.append(" close = \"").append(close).append("\"");
        }
        buf.append(">").append(text).append("</foreach>");
        out.write(buf.toString());
    }

}
