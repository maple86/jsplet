package com.maple86.jsplet.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class SqlTagSupport extends SimpleTagSupport {

    private String id;

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        getJspBody().invoke(writer);
        String text = writer.toString();
        JspWriter out = getJspContext().getOut();
        String buf = "<sql id = \"" + id + "\">" + text + "</sql>";
        out.write(buf);
    }

}
