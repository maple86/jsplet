package com.maple86.jsplet.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class IfTagSupport extends SimpleTagSupport {

    private String test;

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        getJspBody().invoke(writer);
        String text = writer.toString();
        JspWriter out = getJspContext().getOut();
        String buf = "<if test = \"" + test + "\">" + text + "</if>";
        out.write(buf);
    }

}
