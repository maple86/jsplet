package com.maple86.jsplet.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class SetTagSupport extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        getJspBody().invoke(writer);
        String text = writer.toString();
        JspWriter out = getJspContext().getOut();
        out.write("<set>" + text + "</set>");
    }

}
