package com.maple86.jsplet.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class TrimTagSupport extends SimpleTagSupport {

    private String field;

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter writer = new StringWriter();
        getJspBody().invoke(writer);
        String text = writer.toString();
        JspWriter out = getJspContext().getOut();
        StringBuilder buf = new StringBuilder();
        buf.append("<trim");
        if (null != field && !"".equals(field)) {
            buf.append(" field = \"").append(field).append("\"");
        }
        buf.append(">").append(text).append("</trim>");
        out.write(buf.toString());
    }

}
