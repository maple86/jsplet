package com.maple86.jsplet.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class ElementUtil {

    public static Element getElement(Element element, String name) {
        NodeList nodeList = element.getElementsByTagName(name);
        int length = nodeList.getLength();
        for(int i = 0; i < length; i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                return (Element) node;
            }
        }
        return null;
    }

    public static List<Element> getElements(Element element, String name) {
        List<Element> list = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(name);
        int length = nodeList.getLength();
        for(int i = 0; i < length; i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                list.add((Element) node);
            }
        }
        return list;
    }

    public static List<Element> getElements(Element parent) {
        NodeList nodeList = parent.getChildNodes();
        int length = nodeList.getLength();
        List<Element> list = new ArrayList<>(length);
        for(int i = 0; i < length; i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                list.add((Element) node);
            }
        }
        return list;
    }

}
