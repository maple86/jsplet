package com.maple86.jsplet.util;

import com.maple86.jsplet.exception.JspletDaoException;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BeanUtil {

    public static boolean setProperty(Object bean, String key, Object value) {
        String[] fields = key.split("\\.");
        Object target = bean;
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            if (i < fields.length - 1) {
                Object object = getValue(target, field);
                if (null == object) {
                    object = createValue(target, field);
                }
                if (null == object) {
                    return false;
                }
                target = object;
            } else {
                return setValue(target, field, value);
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private static boolean setValue(Object bean, String key, Object value) {
//        if (null == value) {
//            return false;
//        }
        if (bean instanceof Map) {
            ((Map<String, Object>) bean).put(key, value);
            return true;
        }
        Class<?> type = bean.getClass();
        try {
            Field field = type.getField(key);
            Class<?> fieldType = field.getType();
            Object obj = parseType(value, fieldType);
            if (null != obj) {
                field.set(bean, obj);
                return true;
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            // do nothing
        }
        Method method = getSetMethod(type, key);
        if (null == method) {
            return false;
        }
        Class<?>[] parameterTypes = method.getParameterTypes();
        Class<?> parameterType = parameterTypes[0];
        Object obj = value;
        if (obj != null) {
            obj = parseType(value, parameterType);
            if (null == obj) {
                return false;
            }
        }
        try {
            method.invoke(bean, obj);
            return true;
        } catch (IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    private static Method getSetMethod(Class<?> type, String key) {
        Method[] methods = type.getMethods();
        for (Method method : methods) {
            String name = method.getName();
            if (!name.equalsIgnoreCase("set" + key)) {
                continue;
            }
            if (method.getParameterCount() != 1) {
                continue;
            }
            return method;
        }
        return null;
    }

    private static Object createValue(Object bean, String key) {
        Class<?> type = bean.getClass();
        try {
            Field field = type.getField(key);
            Class<?> fieldType = field.getType();
            Object obj = fieldType.newInstance();
            field.set(bean, obj);
            return true;
        }catch (IllegalAccessException | NoSuchFieldException e) {
            // do nothing
        } catch (InstantiationException e) {
            return null;
        }
        Method method = getSetMethod(type, key);
        if (null == method) {
            return null;
        }
        Class<?>[] parameterTypes = method.getParameterTypes();
        Class<?> parameterType = parameterTypes[0];
        try {
            Object value = parameterType.newInstance();
            method.invoke(bean, value);
            return value;
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            return null;
        }
    }

    public static Object getProperty(Object bean, String key) {
        String[] fields = key.split("\\.");
        Object value = bean;
        for (String field : fields) {
            value = getValue(value, field);
            if (null == value) {
                return null;
            }
        }
        return value;
    }

    private static Object getValue(Object bean, String key) {
        if (bean instanceof Map) {
            return ((Map<?, ?>) bean).get(key);
        }
        if ("class".equals(key)) {
            return null;
        }
        Class<?> type = bean.getClass();
        try {
            Field field = type.getField(key);
            return field.get(bean);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            // do nothing
        }
        String name = key.substring(0, 1).toUpperCase() + key.substring(1);
        try {
            Method method = type.getMethod("get" + name);
            return method.invoke(bean);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            // do nothing
        }
        try {
            Method method = type.getMethod("is" + name);
            return method.invoke(bean);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            // do nothing
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T> T parseType(Object value, Class<T> type) {
        if (null == value) {
            return null;
        }
        Class<?> clazz = value.getClass();
        if (type.isAssignableFrom(clazz)) {
            return (T) value;
        }
        if (String.class.equals(type)) {
            return (T) value.toString();
        }
        if (int.class.equals(type) || Integer.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+")) {
                return null;
            }
            return (T) Integer.valueOf(string);
        }
        if (long.class.equals(type) || Long.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+")) {
                return null;
            }
            return (T) Long.valueOf(string);
        }
        if (double.class.equals(type) || Double.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+(\\.\\d+)?")) {
                return null;
            }
            return (T) Double.valueOf(string);
        }
        if (float.class.equals(type) || Float.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+(\\.\\d+)?")) {
                return null;
            }
            return (T) Float.valueOf(string);
        }
        if (short.class.equals(type) || Short.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+(\\.\\d+)?")) {
                return null;
            }
            return (T) Short.valueOf(string);
        }
        if (byte.class.equals(type) || Byte.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+(\\.\\d+)?")) {
                return null;
            }
            return (T) Byte.valueOf(string);
        }
        if (boolean.class.equals(type) || Boolean.class.equals(type)) {
            String string = value.toString();
            if ("true".equals(string)) {
                return (T) Boolean.TRUE;
            } else {
                return (T) Boolean.FALSE;
            }
        }
        if (BigDecimal.class.equals(type)) {
            String string = value.toString();
            if (!string.matches("-?\\d+(\\.\\d+)?")) {
                return null;
            }
            return (T) new BigDecimal(string);
        }
        if (type.isEnum()) {
            String string = value.toString();
            return (T) Enum.valueOf((Class<Enum>)type, string);
        }
        if (Date.class.isAssignableFrom(type)) {
            return parseDate(value, type);
        }
        if (type.isArray()) {
            return parseArray(value, type);
        }
        if (Collection.class.isAssignableFrom(type)) {
            return parseCollection(value, type);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> parseMap(Object bean) {
        if (bean instanceof Map) {
            return (Map<String, Object>) bean;
        }
        Map<String, Object> result = new HashMap<>();
        Class<?> clazz = bean.getClass();
        Field[] fields = clazz.getFields();
        try {
            for (Field field : fields) {
                String name = field.getName();
                Object value = field.get(bean);
                result.put(name, value);
            }
        } catch (IllegalAccessException e) {
            // do nothing
        }
        Method[] methods = clazz.getMethods();
        Pattern pattern = Pattern.compile("(get|is)(.+)");
        try {
            for (Method method : methods) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    continue;
                }
                if (method.getParameterCount() != 0) {
                    continue;
                }
                Matcher matcher = pattern.matcher(name);
                if (!matcher.matches()) {
                    continue;
                }
                String key = matcher.group(2);
                key = key.substring(0, 1).toLowerCase() + key.substring(1);
                Object value = method.invoke(bean);
                result.put(key, value);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            // do nothing
        }
        return result;
    }

    public static <T> T parseBean(Map<String, Object> map, Class<T> type) {
        try {
            T model = type.newInstance();
            Set<Map.Entry<String, Object>> entries = map.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                String key = entry.getKey();
                Object value = entry.getValue();
                BeanUtil.setProperty(model, key, value);
            }
            return model;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new JspletDaoException(e);
        }
    }

    private static <T> T parseDate(Object value, Class<T> type) {
        try {
            if (value instanceof Long) {
                return createDate(type, (Long)value);
            }
            if (value instanceof Date) {
                long time = ((Date) value).getTime();
                return createDate(type, time);
            }
            String string = value.toString();
            if (string.matches("\\d+")) {
                long time = Long.parseLong(string);
                return createDate(type, time);
            }
            DateFormat dateFormat = matchDateFormat(string);
            if (null != dateFormat) {
                long time = dateFormat.parse(string).getTime();
                return createDate(type, time);
            }
            return null;
        } catch (InstantiationException | IllegalAccessException | ParseException | InvocationTargetException e) {
            return null;
        }
    }

    private static DateFormat matchDateFormat(String value) {
        if (value.matches("\\d{1,4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}")) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
        if (value.matches("\\d{1,4}-\\d{1,2}-\\d{1,2}")) {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T> T createDate(Class<T> type, long time) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<?>[] constructors = type.getConstructors();
        for (Constructor<?> constructor : constructors) {
            if (constructor.getParameterCount() == 1) {
                Class<?> parameterType = constructor.getParameterTypes()[0];
                if (long.class.equals(parameterType) || Long.class.equals(parameterType)) {
                    return (T) constructor.newInstance(time);
                }
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T> T parseArray(Object value, Class<T> type) {
        Class<?> componentType = type.getComponentType();
        if (value.getClass().isArray()) {
            int length = Array.getLength(value);
            Object result = Array.newInstance(componentType, length);
            for(int i = 0; i < length; i++) {
                Object item = Array.get(value, i);
                Object component = parseType(item, componentType);
                Array.set(result, i, component);
            }
            return (T) result;
        }
        if (value instanceof Collection) {
            Collection<?> collection = (Collection<?>) value;
            Object result = Array.newInstance(componentType, collection.size());
            int index = 0;
            for (Object item : collection) {
                Object component = parseType(item, componentType);
                Array.set(result, index, component);
                index++;
            }
            return (T) result;
        }
        if (componentType.isInstance(value)) {
            Object result = Array.newInstance(componentType, 1);
            Array.set(result, 0, value);
            return (T) result;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T> T parseCollection(Object value, Class<T> type) {
        try {
            if (value.getClass().isArray()) {
                T result = type.newInstance();
                Collection<? super Object> target = (Collection<? super Object>) result;
                int length = Array.getLength(value);
                for(int i = 0; i < length; i++) {
                    Object item = Array.get(value, i);
                    target.add(item);
                }
                return result;
            }
            if (value instanceof Collection) {
                T result = type.newInstance();
                Collection<? super Object> target = (Collection<? super Object>) result;
                Collection<? super Object> collection = (Collection<? super Object>) value;
                target.addAll(collection);
                return result;
            }
            T result = type.newInstance();
            Collection<? super Object> target = (Collection<? super Object>) result;
            target.add(value);
            return result;
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

}
