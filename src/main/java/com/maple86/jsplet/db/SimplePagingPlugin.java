package com.maple86.jsplet.db;

import com.maple86.jsplet.api.PagingPlugin;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

public class SimplePagingPlugin implements PagingPlugin {

    @Override
    public boolean accept(DatabaseMetaData metaData) throws SQLException {
        String productName = metaData.getDatabaseProductName();
        if ("H2".equalsIgnoreCase(productName)) {
            return true;
        }
        if ("HSQL Database Engine".equalsIgnoreCase(productName)) {
            return true;
        }
        return "MySql".equalsIgnoreCase(productName);
    }

    @Override
    public String parseSql(String sql, int offset, int limit) {
        return sql + " LIMIT " + offset + ", " + limit;
    }

}
