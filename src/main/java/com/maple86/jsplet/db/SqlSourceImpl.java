package com.maple86.jsplet.db;

import com.maple86.jsplet.api.SqlSource;
import com.maple86.jsplet.exception.JspletDaoException;
import org.apache.taglibs.standard.lang.jstl.ELEvaluator;
import org.apache.taglibs.standard.lang.jstl.ELException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlSourceImpl implements SqlSource {

    private final Element element;

    public SqlSourceImpl(Element element) {
        this.element = element;
    }

    @Override
    public SqlData parseSql(Map<String, Object> paramMap) {
        List<Object> list = new ArrayList<>();
        String sql = parseChildNodes(element, paramMap, list);
        sql = sql.replaceAll("\\s+", " ");
        sql = sql.trim();
        return new SqlData(sql, list.toArray());
    }

    private String parseChildNodes(Element ele, Map<String, Object> paramMap, List<Object> list) {
        NodeList childNodes = ele.getChildNodes();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            String rst = parseNode(node, paramMap, list);
            buf.append(rst);
        }
        return buf.toString();
    }

    private String parseNode(Node node, Map<String, Object> paramMap, List<Object> list) {
        StringBuilder buf = new StringBuilder();
        short type = node.getNodeType();
        if (type == Node.TEXT_NODE) {
            String text = node.getTextContent();
            text = parseVariable(text, paramMap, list);
            buf.append(text);
        } else if (type == Node.ELEMENT_NODE) {
            Element ele = (Element) node;
            String rst = parseElement(ele, paramMap, list);
            buf.append(rst);
        }
        return buf.toString();
    }

    private String parseVariable(String sql, Map<String, Object> paramMap, List<Object> list) {
        Pattern pattern = Pattern.compile("(\\$\\{?)([a-zA-Z0-9\\-_.]+)(}?)");
        Matcher matcher = pattern.matcher(sql);
        while (matcher.find()) {
            String key = matcher.group(2);
            Object value = getVariable(paramMap, key);
            if (null == value) {
                throw new JspletDaoException("属性不存在：" + key);
            }
            sql = sql.replaceFirst("(\\$\\{?)([a-zA-Z0-9\\-_.]+)(}?)", value.toString());
        }
        pattern = Pattern.compile("(#\\{?)([a-zA-Z0-9\\-_.]+)(})?");
        matcher = pattern.matcher(sql);
        while (matcher.find()) {
            String key = matcher.group(2);
            Object value = getVariable(paramMap, key);
            if (null == value) {
                throw new JspletDaoException("属性不存在：" + key);
            }
            list.add(value);
        }
        sql = sql.replaceAll("(#\\{?)([a-zA-Z0-9\\-_.]+)(}?)", " ? ");
        return sql;
    }

    private Object getVariable(Map<String, Object> paramMap, String key) {
        String express = "${" + key + "}";
        ELEvaluator elEvaluator = new ELEvaluator((name, context) -> ((Map<?, ?>)context).get(name));
        try {
            return elEvaluator.evaluate(express, paramMap, Object.class, null, null);
        } catch (ELException ex) {
            throw new JspletDaoException(ex);
        }
    }

    private String parseElement(Element ele, Map<String, Object> paramMap, List<Object> list) {
        StringBuilder buf = new StringBuilder();
        String tagName = ele.getTagName();
        if ("where".equals(tagName)) {
            String rst = parseWhere(ele, paramMap, list);
            buf.append(rst);
        } else if ("if".equals(tagName)) {
            String rst = parseIf(ele, paramMap, list);
            buf.append(rst);
        } else if ("trim".equals(tagName)) {
            String rst = parseTrim(ele, paramMap, list);
            buf.append(rst);
        } else if ("set".equals(tagName)) {
            String rst = parseSet(ele, paramMap, list);
            buf.append(rst);
        } else if ("foreach".equals(tagName)) {
            String rst = parseForEach(ele, paramMap, list);
            buf.append(rst);
        }
        return buf.toString();
    }

    private String parseWhere(Element ele, Map<String, Object> paramMap, List<Object> list) {
        String sql = parseChildNodes(ele, paramMap, list);
        sql = sql.replaceFirst("^\\s*and\\s+", " ");
        sql = sql.replaceFirst("^\\s*or\\s+", " ");
        sql = sql.replaceFirst("^\\s*AND\\s+", " ");
        sql = sql.replaceFirst("^\\s*OR\\s+", " ");
        sql = sql.trim();
        if (!"".equals(sql)) {
            sql = " where " + sql;
        }
        return sql;
    }

    private String parseIf(Element ele, Map<String, Object> paramMap, List<Object> list) {
        String test = ele.getAttribute("test");
        String express = "${" + test + "}";
        ELEvaluator elEvaluator = new ELEvaluator((name, context) -> ((Map<?, ?>)context).get(name));
        try {
            Object value = elEvaluator.evaluate(express, paramMap, Boolean.class, null, null);
            if (Boolean.TRUE.equals(value)) {
                return parseChildNodes(ele, paramMap, list);
            }
        } catch (ELException ex) {
            throw new JspletDaoException(ex);
        }
        return "";
    }

    private String parseTrim(Element ele, Map<String, Object> paramMap, List<Object> list) {
        String fields = ele.getAttribute("fields");
        if (null == fields || "".equals(fields)) {
            synchronized (this) {
                fields = ele.getAttribute("fields");
                if (null == fields || "".equals(fields)) {
                    fields = findFields(ele);
                    ele.setAttribute("fields", fields);
                }
            }
        }
        String[] array = fields.split(",");
        for (String field : array) {
            Object value = paramMap.get(field.trim());
            if (null == value || "".equals(value)) {
                return "";
            }
        }
        return parseChildNodes(ele, paramMap, list);
    }

    private String findFields(Element ele) {
        String text = ele.getTextContent();
        Pattern pattern = Pattern.compile("([$#]\\{?)([a-zA-Z0-9\\-_]+)(}?)");
        Matcher matcher = pattern.matcher(text);
        StringBuilder buf = new StringBuilder();
        while (matcher.find()) {
            String field = matcher.group(2);
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(field);
        }
        String fields = buf.toString();
        if ("".equals(fields)) {
            throw new JspletDaoException("ne标签内未发现变量，请填入fields属性");
        }
        return fields;
    }

    private String parseSet(Element ele, Map<String, Object> paramMap, List<Object> list) {
        String sql = parseChildNodes(ele, paramMap, list);
        sql = sql.replaceFirst("^\\s*,", " ");
        if (!"".equals(sql.trim())) {
            sql = " set " + sql;
        }
        return sql;
    }

    private String parseForEach(Element ele, Map<String, Object> paramMap, List<Object> list) {
        String collection = ele.getAttribute("collection");
        String itemName = ele.getAttribute("item");
        String open = ele.getAttribute("open");
        if (null == open) {
            open = "";
        }
        String close = ele.getAttribute("close");
        if (null == close) {
            close = "";
        }
        String separator = ele.getAttribute("separator");
        if (null == separator) {
            separator = "";
        }
        Object values = paramMap.get(collection);
        if (null == values) {
            return "";
        }
        Collection<?> items = castCollection(values);
        if (items.isEmpty()) {
            return "";
        }
        Map<String, Object> map = new HashMap<>(paramMap);
        StringBuilder buf = new StringBuilder();
        buf.append(open);
        boolean first = true;
        for (Object item : items) {
            if (null != itemName && !"".equals(itemName)) {
                map.put(itemName, item);
            }
            if (!first) {
                buf.append(separator);
            }
            String sql = parseChildNodes(ele, map, list);
            buf.append(sql);
            first = false;
        }
        buf.append(close);
        return buf.toString();
    }

    private Collection<?> castCollection(Object target) {
        if (target instanceof Collection) {
            return (Collection<?>) target;
        }
        if (target.getClass().isArray()) {
            int length = Array.getLength(target);
            List<Object> list = new ArrayList<>(length);
            for (int i = 0; i < length; i++) {
                Object value = Array.get(target, i);
                list.add(value);
            }
            return list;
        }
        throw new JspletDaoException("对象不是数组或者集合类型：" + target.getClass());
    }

}
