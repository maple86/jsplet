package com.maple86.jsplet.db;

import com.maple86.jsplet.api.DataSession;
import com.maple86.jsplet.api.DataSessionFactory;
import com.maple86.jsplet.api.JspletEngine;

public class DataSessionFactoryImpl implements DataSessionFactory {

    private final JspletEngine jspletEngine;

    private final ThreadLocal<DataSession> sessionThreadLocal = new ThreadLocal<>();

    public DataSessionFactoryImpl(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @Override
    public boolean hasSession() {
        DataSession dataSession = sessionThreadLocal.get();
        return null != dataSession;
    }

    @Override
    public DataSession openSession() {
        DataSession dataSession = sessionThreadLocal.get();
        if (null != dataSession) {
            return dataSession;
        }
        DataSessionImpl session = new DataSessionImpl(jspletEngine, sessionThreadLocal::remove);
        sessionThreadLocal.set(session);
        return session;
    }

    @Override
    public void closeSession() {
        DataSession dataSession = sessionThreadLocal.get();
        if (null == dataSession) {
            return;
        }
        dataSession.close();
    }
}
