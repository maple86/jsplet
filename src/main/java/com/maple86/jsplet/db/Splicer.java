package com.maple86.jsplet.db;

import com.maple86.jsplet.annotation.Inquirer;
import com.maple86.jsplet.api.InquirerMarker;

import java.io.Serializable;

@Inquirer(SplicerInquirerHandler.class)
public class Splicer implements InquirerMarker, Serializable {

    private final String content;

    private Object[] params;

    public Splicer(String content, Object[] params) {
        this.content = content;
        this.params = params;
    }

    public Splicer(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public Object[] getParams() {
        return params;
    }
}
