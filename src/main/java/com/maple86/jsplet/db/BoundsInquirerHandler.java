package com.maple86.jsplet.db;

import com.maple86.jsplet.api.*;
import com.maple86.jsplet.exception.JspletDaoException;
import com.maple86.jsplet.map.TypeMap;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.List;

public class BoundsInquirerHandler implements InquirerHandler<RowBounds> {

    private JspletEngine jspletEngine;

    private String dataSource;

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @Override
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<TypeMap> select(RowBounds inquirer, SqlData sqlData) {
        try {
            String sql = sqlData.getSql();
            Object[] params = sqlData.getParams();
            DataSession dataSession = getDataSession();
            DatabaseMetaData metaData = dataSession.getDatabaseMetaData(dataSource);
            PagingPlugin pagingPlugin = accept(metaData);
            sql = pagingPlugin.parseSql(sql, inquirer.getOffset(), inquirer.getLimit());
            SqlData contentSqlData = new SqlData(sql, params);
            return dataSession.select(dataSource, contentSqlData);
        } catch (SQLException e) {
            throw new JspletDaoException(e);
        }
    }

    private PagingPlugin accept(DatabaseMetaData metaData) throws SQLException {
        List<PagingPlugin> pagingPlugins = jspletEngine.getBeans(PagingPlugin.class);
        for(int i = pagingPlugins.size() - 1; i >= 0; i--) {
            PagingPlugin pagingPlugin = pagingPlugins.get(i);
            if (pagingPlugin.accept(metaData)) {
                return pagingPlugin;
            }
        }
        String productName = metaData.getDatabaseProductName();
        throw new JspletDaoException("暂不支持的数据库类型：" + productName);
    }

    private DataSession getDataSession() {
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        return dataSessionFactory.openSession();
    }

}
