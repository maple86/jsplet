package com.maple86.jsplet.db;

import com.maple86.jsplet.exception.JspletDaoException;
import com.maple86.jsplet.map.TypeMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataConnection {

    private final Log log = LogFactory.getLog(this.getClass());

    private final String dataSourceName;

    private final DataSource dataSource;

    private Connection connection;

    private boolean transaction;

    private TransactionLevel transactionLevel;

    private boolean closed;

    private Map<SqlData, List<TypeMap>> cacheMap = new HashMap<>();

    public DataConnection(String dataSourceName, DataSource dataSource) {
        this.dataSourceName = dataSourceName;
        this.dataSource = dataSource;
    }

    public void openTransaction(TransactionLevel level) {
        try {
            if (transaction) {
                return;
            }
            transaction = true;
            this.transactionLevel = level;
            if (null != connection && !connection.isClosed()) {
                connection.setAutoCommit(false);
                connection.setTransactionIsolation(transactionLevel.value);
            }
        } catch (SQLException ex) {
            throw new JspletDaoException(ex);
        }
    }

    public Connection getConnection() {
        if (closed) {
            throw new JspletDaoException("DatabaseSession已关闭！");
        }
        try {
            if (null != connection && !connection.isClosed()) {
                return connection;
            }
            log.trace("开启数据库连接，数据源：" + dataSourceName);
            connection = dataSource.getConnection();
            if (transaction) {
                log.trace("开启数据库事务，数据源：" + dataSourceName);
                connection.setAutoCommit(false);
                connection.setTransactionIsolation(transactionLevel.value);
            }
            return connection;
        } catch (SQLException ex) {
            throw new JspletDaoException(ex);
        }
    }

    public Map<SqlData, List<TypeMap>> getCacheMap() {
        return cacheMap;
    }

    public void close() {
        closed = true;
        cacheMap.clear();
        if (null == connection) {
            return;
        }
        try {
            if (connection.isClosed()) {
                connection = null;
                return;
            }
            log.trace("关闭数据库连接，数据源：" + dataSourceName);
            connection.close();
            connection = null;
        } catch (SQLException ex) {
            throw new JspletDaoException(ex);
        }
    }

    public void commit() {
        if (!transaction) {
            return;
        }
        transaction = false;
        if (closed) {
            return;
        }
        if (null == connection) {
            return;
        }
        try {
            if (connection.isClosed()) {
                return;
            }
            log.trace("提交数据事务，数据源：" + dataSourceName);
            connection.commit();
            log.trace("关闭数据连接，数据源：" + dataSourceName);
            connection.close();
        } catch (SQLException ex) {
            throw new JspletDaoException(ex);
        }
    }

    public void rollback() {
        if (!transaction) {
            return;
        }
        transaction = false;
        if (closed) {
            return;
        }
        if (null == connection) {
            return;
        }
        try {
            if (connection.isClosed()) {
                return;
            }
            log.trace("数据库事务回滚，数据源：" + dataSourceName);
            connection.rollback();
            log.trace("关闭数据库事务，数据源：" + dataSourceName);
            connection.close();
        } catch (SQLException ex) {
            throw new JspletDaoException(ex);
        }
    }

}
