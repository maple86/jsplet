package com.maple86.jsplet.db;

import com.maple86.jsplet.annotation.Inquirer;
import com.maple86.jsplet.api.InquirerMarker;

import java.io.Serializable;

@Inquirer(BoundsInquirerHandler.class)
public class RowBounds implements InquirerMarker, Serializable {

    private final int offset;

    private final int limit;

    public RowBounds(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

}
