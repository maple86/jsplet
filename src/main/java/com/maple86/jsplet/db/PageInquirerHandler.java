package com.maple86.jsplet.db;

import com.maple86.jsplet.api.*;
import com.maple86.jsplet.exception.JspletDaoException;
import com.maple86.jsplet.map.TypeMap;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PageInquirerHandler implements InquirerHandler<PageInfo> {

    private JspletEngine jspletEngine;

    private String dataSource;

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @Override
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<TypeMap> select(PageInfo inquirer, SqlData sqlData) {
        SqlData countSqlData = generateCount(sqlData);
        return select(inquirer, sqlData, countSqlData);
    }

    private SqlData generateCount(SqlData sqlData) {
        String sql = sqlData.getSql();
        Object[] params = sqlData.getParams();
        sql = sql.replaceAll("([,()=<>])", " $1 ");
        String[] words = sql.split("\\s+");
        boolean wrap = hasGroup(words);
        words = filterOrderBy(words);
        sql = parseCount(words, wrap);
        int count = 0;
        for(int i = 0; i < sql.length(); i++) {
            char ch = sql.charAt(i);
            if (ch == '?') {
                count++;
            }
        }
        Object[] ps = params;
        if (count < params.length) {
            ps = new Object[count];
            System.arraycopy(params,params.length - count, ps, 0, count);
        }
        return new SqlData(sql, ps);
    }

    private String[] filterOrderBy(String[] words) {
        List<String> list = new ArrayList<>();
        int temp = 0;
        for (String word : words) {
            if (word.equals("(")) {
                temp++;
            } else if (word.equals(")")) {
                temp--;
            } else if (temp == 0 && "ORDER".equalsIgnoreCase(word)) {
                break;
            }
            list.add(word);
        }
        return list.toArray(new String[0]);
    }

    private String parseCount(String[] words, boolean wrap) {
        StringBuilder buf = new StringBuilder();
        if (wrap) {
            buf.append("SELECT COUNT(*) FROM ( SELECT 1 FROM");
        } else {
            buf.append("SELECT COUNT(*) FROM");
        }
        int temp = 0;
        boolean flag = false;
        for (String word : words) {
            if (!flag) {
                if (word.equals("(")) {
                    temp++;
                } else if (word.equals(")")) {
                    temp--;
                } else if (temp == 0 && "FROM".equalsIgnoreCase(word)) {
                    flag = true;
                }
            } else {
                if (!",".equals(word) && !"(".equals(word)) {
                    buf.append(" ");
                }
                buf.append(word);
            }
        }
        if (wrap) {
            buf.append(" ) _TEMP_");
        }
        String sql = buf.toString();
        sql = sql.replaceAll("([<>])\\s+([>=])", "$1$2");
        return sql;
    }

    private boolean hasGroup(String[] words) {
        int temp = 0;
        for (String word : words) {
            if (word.equals("(")) {
                temp++;
            } else if (word.equals(")")) {
                temp--;
            } else if (temp == 0 && word.equalsIgnoreCase("GROUP")) {
                return true;
            }
        }
        return false;
    }

    private List<TypeMap> select(PageInfo pageInfo, SqlData sqlData, SqlData countSqlData) {
        DataSession dataSession = getDataSession();
        List<TypeMap> list = dataSession.select(dataSource, countSqlData);
        if (list.size() != 1) {
            throw new JspletDaoException("查询错误！");
        }
        TypeMap map = list.get(0);
        Iterator<String> iter = map.keySet().iterator();
        String key = iter.next();
        Integer totalCount = map.get(key, Integer.class);
        pageInfo.calculate(totalCount);
        SqlData contentSqlData = parseSql(sqlData, pageInfo);
        return dataSession.select(dataSource, contentSqlData);
    }

    private SqlData parseSql(SqlData sqlData, PageInfo pageInfo) {
        try {
            String sql = sqlData.getSql();
            Object[] params = sqlData.getParams();
            DataSession dataSession = getDataSession();
            DatabaseMetaData metaData = dataSession.getDatabaseMetaData(dataSource);
            PagingPlugin pagingPlugin = accept(metaData);
            sql = pagingPlugin.parseSql(sql, pageInfo.getStart(), pageInfo.getPageSize());
            return new SqlData(sql, params);
        } catch (SQLException e) {
            throw new JspletDaoException(e);
        }
    }

    private PagingPlugin accept(DatabaseMetaData metaData) throws SQLException {
        List<PagingPlugin> pagingPlugins = jspletEngine.getBeans(PagingPlugin.class);
        for(int i = pagingPlugins.size() - 1; i >= 0; i--) {
            PagingPlugin pagingPlugin = pagingPlugins.get(i);
            if (pagingPlugin.accept(metaData)) {
                return pagingPlugin;
            }
        }
        String productName = metaData.getDatabaseProductName();
        throw new JspletDaoException("暂不支持的数据库类型：" + productName);
    }

    private DataSession getDataSession() {
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        return dataSessionFactory.openSession();
    }

}
