package com.maple86.jsplet.db;

public enum TransactionLevel {

    TRANSACTION_DEFAULT(-1),

    TRANSACTION_NONE(0),

    TRANSACTION_READ_UNCOMMITTED(1),

    TRANSACTION_READ_COMMITTED(2),

    TRANSACTION_REPEATABLE_READ(4),

    TRANSACTION_SERIALIZABLE(8);

    public final int value;

    TransactionLevel(int value) {
        this.value = value;
    }

    public static TransactionLevel valueOf(int value) {
        if (value == TRANSACTION_NONE.value) {
            return TRANSACTION_NONE;
        } else if (value == TRANSACTION_READ_UNCOMMITTED.value) {
            return TRANSACTION_READ_UNCOMMITTED;
        } else if (value == TRANSACTION_READ_COMMITTED.value) {
            return TRANSACTION_READ_COMMITTED;
        } else if (value == TRANSACTION_REPEATABLE_READ.value) {
            return TRANSACTION_REPEATABLE_READ;
        } else if (value == TRANSACTION_SERIALIZABLE.value) {
            return TRANSACTION_SERIALIZABLE;
        }
        return null;
    }

}
