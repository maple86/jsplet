package com.maple86.jsplet.db;

import com.maple86.jsplet.api.DataSession;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.exception.JspletDaoException;
import com.maple86.jsplet.map.HashTypeMap;
import com.maple86.jsplet.map.TypeMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.util.*;

public class DataSessionImpl implements DataSession {

    private final Log log = LogFactory.getLog(this.getClass());

    private final JspletEngine jspletEngine;

    private boolean transaction;

    private TransactionLevel transactionLevel;

    private boolean closed;

    private final Map<String, DataConnection> connectionMap = new HashMap<>();

    private final Runnable closeHandler;

    public DataSessionImpl(JspletEngine jspletEngine, Runnable closeHandler) {
        this.jspletEngine = jspletEngine;
        this.closeHandler = closeHandler;
        log.debug("DataSession开启");
    }

    @Override
    public boolean hasTransaction() {
        return transaction;
    }

    @Override
    public void openTransaction(TransactionLevel level) {
        if (transaction) {
            return;
        }
        log.debug("DataSession事务开启");
        transaction = true;
        transactionLevel = level;
        connectionMap.forEach((key, conn) -> conn.openTransaction(level));
    }

    private Connection getConnection(String dataSource) {
        DataConnection dataConnection = getDataConnection(dataSource);
        return dataConnection.getConnection();
    }

    private Map<SqlData, List<TypeMap>> getCacheMap(String dataSource) {
        DataConnection dataConnection = getDataConnection(dataSource);
        return dataConnection.getCacheMap();
    }

    private DataConnection getDataConnection(String dataSource) {
        if (null == dataSource || "".equals(dataSource)) {
            throw new JspletDaoException("模块未配置数据源");
        }
        DataConnection dataConnection = connectionMap.get(dataSource);
        if (null == dataConnection) {
            DataSource source = (DataSource) jspletEngine.getBean(dataSource);
            if (source == null) {
                throw new JspletDaoException("未找到数据源：" + dataSource);
            }
            dataConnection = new DataConnection(dataSource, source);
            if (transaction) {
                dataConnection.openTransaction(transactionLevel);
            }
            connectionMap.put(dataSource, dataConnection);
        }
        return dataConnection;
    }

    @Override
    public void close() {
        if (closed) {
            return;
        }
        log.debug("DataSession关闭");
        connectionMap.forEach((key, conn) -> conn.close());
        closed = true;
        closeHandler.run();
    }

    @Override
    public void commit() {
        if (!transaction) {
            return;
        }
        log.debug("DataSession事务提交");
        connectionMap.forEach((key, conn) -> conn.commit());
        transaction = false;
    }

    @Override
    public void rollback() {
        if (!transaction) {
            return;
        }
        log.debug("DataSession事务回滚");
        connectionMap.forEach((key, conn) -> conn.rollback());
        transaction = false;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public List<TypeMap> insert(String dataSource, SqlData sqlData) {
        getCacheMap(dataSource).clear();
        Connection connection = getConnection(dataSource);
        printSql(sqlData);
        try (PreparedStatement statement = createPreparedStatement(connection, sqlData, true)) {
            int count = statement.executeUpdate();
            log.trace("执行结果影响" + count + "行");
            ResultSet resultSet = statement.getGeneratedKeys();
            ResultSetMetaData metaData = resultSet.getMetaData();
            List<TypeMap> list = new ArrayList<>();
            while (resultSet.next()) {
                int size = metaData.getColumnCount();
                TypeMap map = new HashTypeMap();
                for (int i = 0; i < size; i++) {
                    String columnName = metaData.getColumnLabel(i + 1);
                    Object value = resultSet.getObject(i + 1);
                    map.put(columnName, value);
                }
                list.add(map);
            }
            if (list.isEmpty()) {
                for(int i = 0; i < count; i++) {
                    list.add(new HashTypeMap());
                }
            }
            return list;
        } catch (SQLException e) {
            throw new JspletDaoException(e);
        } finally {
            closeConnection(dataSource, connection);
        }
    }

    @Override
    public int delete(String dataSource, SqlData sqlData) {
        return executeUpdate(dataSource, sqlData);
    }

    @Override
    public int update(String dataSource, SqlData sqlData) {
        return executeUpdate(dataSource, sqlData);
    }

    @Override
    public List<TypeMap> select(String dataSource, SqlData sqlData) {
        Map<SqlData, List<TypeMap>> cacheMap = getCacheMap(dataSource);
        List<TypeMap> maps = cacheMap.get(sqlData);
        if (null != maps) {
            printSql(sqlData);
            log.debug("读取缓存");
            return copyMapList(maps);
        }
        Connection connection = getConnection(dataSource);
        printSql(sqlData);
        try (PreparedStatement statement = createPreparedStatement(connection, sqlData, false);
             ResultSet resultSet = statement.executeQuery()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            List<TypeMap> list = new ArrayList<>();
            while (resultSet.next()) {
                int count = metaData.getColumnCount();
                TypeMap map = new HashTypeMap();
                Object[] values = new Object[count];
                for (int i = 0; i < count; i++) {
                    String columnName = metaData.getColumnLabel(i + 1);
                    Object value = resultSet.getObject(i + 1);
                    value = parseValue(value);
                    map.put(columnName, value);
                    values[i] = value;
                }
                if (log.isTraceEnabled()) {
                    log.trace("查询结果：" + parseArray(values));
                }
                list.add(map);
            }
            cacheMap.put(sqlData, list);
            log.debug("查询出" + list.size() + "条数据");
            return list;
        } catch (SQLException | IOException e) {
            throw new JspletDaoException(e);
        } finally {
            closeConnection(dataSource, connection);
        }
    }

    private Object parseValue(Object value) throws SQLException, IOException {
        if (value instanceof Clob) {
            Clob clob = (Clob) value;
            char[] cs = new char[1024];
            StringBuilder buf = new StringBuilder();
            try (Reader reader = clob.getCharacterStream()) {
                int len;
                while((len = reader.read(cs)) > 0) {
                    buf.append(cs, 0, len);
                }
            }
            return buf.toString();
        }
        return value;
    }

    @Override
    public DatabaseMetaData getDatabaseMetaData(String dataSource) {
        Connection connection = getConnection(dataSource);
        try {
            return connection.getMetaData();
        } catch (SQLException e) {
            throw new JspletDaoException(e);
        }
    }

    private int executeUpdate(String dataSource, SqlData sqlData) {
        getCacheMap(dataSource).clear();
        Connection connection = getConnection(dataSource);
        printSql(sqlData);
        try (PreparedStatement statement = createPreparedStatement(connection, sqlData, false)) {
            int count = statement.executeUpdate();
            log.debug("执行结果影响" + count + "行");
            return count;
        } catch (SQLException e) {
            throw new JspletDaoException(e);
        } finally {
            closeConnection(dataSource, connection);
        }
    }

    private PreparedStatement createPreparedStatement(Connection connection, SqlData sqlData, boolean returnKey) throws SQLException {
        String sql = sqlData.getSql();
        Object[] params = sqlData.getParams();
        PreparedStatement statement;
        if (returnKey) {
            statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        } else {
            statement = connection.prepareStatement(sql);
        }
        for(int i = 0; i < params.length; i++) {
            Object param = params[i];
            statement.setObject(i + 1, param);
        }
        return statement;
    }

    private void closeConnection(String dataSource, Connection connection) {
        if (!hasTransaction()) {
            try {
                log.trace("关闭数据库连接，数据源：" + dataSource);
                connection.close();
            } catch (SQLException ex) {
                throw new JspletDaoException(ex);
            }
        }
    }

    private List<TypeMap> copyMapList(List<TypeMap> list) {
        List<TypeMap> result = new ArrayList<>(list.size());
        for (TypeMap typeMap : list) {
            TypeMap map = new HashTypeMap();
            map.putAll(typeMap);
            result.add(map);
        }
        return result;
    }

    private void printSql(SqlData sqlData) {
        if (!log.isDebugEnabled()) {
            return;
        }
        String sql = sqlData.getSql();
        Object[] params = sqlData.getParams();
        StringBuilder buf = new StringBuilder();
        buf.append("执行sql语句：");
        buf.append("\r\n[JSPLET] ==>  Preparing: ");
        buf.append(sql);
        buf.append("\r\n[JSPLET] ==> Parameters: ");
        String text = parseArray(params);
        buf.append(text);
        log.debug(buf.toString());
    }

    private String parseArray(Object[] values) {
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        for(int i = 0; i < values.length; i++) {
            if (i > 0) {
                buf.append(", ");
            }
            String text = String.valueOf(values[i]);
            if (text.length() > 50) {
                buf.append(text, 0, 50).append("...");
            } else {
                buf.append(text);
            }
        }
        buf.append("]");
        return buf.toString();
    }

}
