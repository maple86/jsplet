package com.maple86.jsplet.db;

import com.maple86.jsplet.api.DataSessionFactory;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.reflect.MethodInvoker;

import java.lang.reflect.Method;

public class CloseSessionInvoker implements MethodInvoker {

    private final JspletEngine jspletEngine;

    private final MethodInvoker methodInvoker;

    public CloseSessionInvoker(JspletEngine jspletEngine, MethodInvoker methodInvoker) {
        this.jspletEngine = jspletEngine;
        this.methodInvoker = methodInvoker;
    }

    @Override
    public Object invoke(Object target, Method method, Object... arguments) throws Throwable {
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        if (dataSessionFactory.hasSession()) {
            // 如果执行前已经存在session，则交由外部处理
            return methodInvoker.invoke(target, method, arguments);
        } else {
            try {
                return methodInvoker.invoke(target, method, arguments);
            } finally {
                // 如果执行前不存在session，执行后存在session，则关闭session
                if (dataSessionFactory.hasSession()) {
                    dataSessionFactory.closeSession();
                }
            }
        }
    }

}
