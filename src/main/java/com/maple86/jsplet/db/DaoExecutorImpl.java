package com.maple86.jsplet.db;

import com.maple86.jsplet.annotation.Inquirer;
import com.maple86.jsplet.api.*;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.exception.JspletDaoException;
import com.maple86.jsplet.map.HashTypeMap;
import com.maple86.jsplet.map.TypeMap;
import com.maple86.jsplet.util.BeanUtil;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DaoExecutorImpl implements DaoExecutor {

    private final JspletModule jspletModule;

    private final SqlSource sqlSource;

    private final Map<String, Object> paramMap = new HashMap<>();

    public DaoExecutorImpl(JspletModule jspletModule, SqlSource sqlSource) {
        this.jspletModule = jspletModule;
        this.sqlSource = sqlSource;
    }

    @Override
    public DaoExecutor put(String name, Object value) {
        paramMap.put(name, value);
        return this;
    }

    @Override
    public DaoExecutor putAll(Object model) {
        Map<String, Object> map = BeanUtil.parseMap(model);
        map.forEach(this::put);
        return this;
    }

    @Override
    public List<TypeMap> select() {
        SqlData sqlData = sqlSource.parseSql(paramMap);
        String dataSource = jspletModule.getDataSource();
        DataSession dataSession = getDataSession();
        List<TypeMap> list = dataSession.select(dataSource, sqlData);
        list = camelCases(list);
        return list;
    }

    @Override
    public <T> List<T> select(Class<T> type) {
        List<TypeMap> list = select();
        return parseList(list, type);
    }

    @Override
    public List<TypeMap> select(InquirerMarker inquirer) {
        if (null == inquirer) {
            throw new JspletDaoException("参数不能为空！");
        }
        Class<?> clazz = inquirer.getClass();
        Inquirer annotation = clazz.getAnnotation(Inquirer.class);
        if (null == annotation) {
            throw new JspletDaoException("分页对象未添加注解：" + Inquirer.class.getName());
        }
        Class<? extends InquirerHandler<? extends InquirerMarker>> type = annotation.value();
        try {
            InquirerHandler<?> inquirerHandler = type.newInstance();
            JspletEngine jspletEngine = jspletModule.getJspletEngine();
            String dataSource = jspletModule.getDataSource();
            inquirerHandler.setJspletEngine(jspletEngine);
            inquirerHandler.setDataSource(dataSource);
            SqlData sqlData = sqlSource.parseSql(paramMap);
            List<TypeMap> list = selectInquirer(inquirerHandler, inquirer, sqlData);
            list = camelCases(list);
            return list;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new JspletDaoException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    private <E extends InquirerMarker> List<TypeMap> selectInquirer(InquirerHandler<E> inquirerHandler, InquirerMarker inquirer, SqlData sqlData) {
        E inquirerMarker = (E) inquirer;
        return inquirerHandler.select(inquirerMarker, sqlData);
    }

    @Override
    public <T> List<T> select(Class<T> type, InquirerMarker inquirer) {
        List<TypeMap> list = select(inquirer);
        return parseList(list, type);
    }

    @Override
    public TypeMap selectOne(InquirerMarker inquirer) {
        List<TypeMap> list = select(inquirer);
        if (list.isEmpty()) {
            return null;
        }
        if (list.size() > 1) {
            throw new JspletDaoException("结果超过一个！");
        }
        return list.get(0);
    }

    @Override
    public TypeMap selectOne() {
        List<TypeMap> list = select();
        if (list.isEmpty()) {
            return null;
        }
        if (list.size() > 1) {
            throw new JspletDaoException("结果超过一个！");
        }
        return list.get(0);
    }

    @Override
    public <T> T selectOne(Class<T> type) {
        TypeMap map = selectOne();
        return BeanUtil.parseBean(map, type);
    }

    @Override
    public <T> T selectOne(Class<T> type, InquirerMarker inquirer) {
        TypeMap map = selectOne(inquirer);
        return BeanUtil.parseBean(map, type);
    }

    @Override
    public Object selectValue() {
        TypeMap map = selectOne();
        if (null == map) {
            return null;
        }
        if (map.isEmpty()) {
            throw new JspletDaoException("返回结果字段不正确！");
        }
        if (map.size() != 1) {
            throw new JspletDaoException("返回结果包含多个字段！");
        }
        Iterator<Object> iterator = map.values().iterator();
        return iterator.next();
    }

    @Override
    public Object selectValue(InquirerMarker inquirer) {
        TypeMap map = selectOne(inquirer);
        if (null == map) {
            return null;
        }
        if (map.isEmpty()) {
            throw new JspletDaoException("返回结果字段不正确！");
        }
        if (map.size() != 1) {
            throw new JspletDaoException("返回结果包含多个字段！");
        }
        Iterator<Object> iterator = map.values().iterator();
        return iterator.next();
    }

    @Override
    public <T> T selectValue(Class<T> type) {
        Object value = selectValue();
        return BeanUtil.parseType(value, type);
    }

    @Override
    public <T> T selectValue(Class<T> type, InquirerMarker inquirer) {
        Object value = selectValue(inquirer);
        return BeanUtil.parseType(value, type);
    }

    @Override
    public List<TypeMap> insert() {
        SqlData sqlData = sqlSource.parseSql(paramMap);
        String dataSource = jspletModule.getDataSource();
        DataSession dataSession = getDataSession();
        List<TypeMap> list = dataSession.insert(dataSource, sqlData);
        list = camelCases(list);
        return list;
    }

    @Override
    public TypeMap insertOne() {
        List<TypeMap> list = insert();
        if (list.isEmpty()) {
            return null;
        }
        if (list.size() > 1) {
            throw new JspletDaoException("结果超过一个！");
        }
        return list.get(0);
    }

    @Override
    public int update() {
        SqlData sqlData = sqlSource.parseSql(paramMap);
        String dataSource = jspletModule.getDataSource();
        DataSession dataSession = getDataSession();
        return dataSession.update(dataSource, sqlData);
    }

    @Override
    public int delete() {
        SqlData sqlData = sqlSource.parseSql(paramMap);
        String dataSource = jspletModule.getDataSource();
        DataSession dataSession = getDataSession();
        return dataSession.delete(dataSource, sqlData);
    }

    private <T> List<T> parseList(List<TypeMap> list, Class<T> type) {
        List<T> result = new ArrayList<>(list.size());
        for (TypeMap map : list) {
            T model = BeanUtil.parseBean(map, type);
            result.add(model);
        }
        return result;
    }

    private DataSession getDataSession() {
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        return dataSessionFactory.openSession();
    }

    private List<TypeMap> camelCases(List<TypeMap> list) {
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        boolean mapUnderscoreToCamelCase = jspletSetting.isMapUnderscoreToCamelCase();
        if (!mapUnderscoreToCamelCase) {
            return list;
        }
        List<TypeMap> result = new ArrayList<>(list.size());
        for (TypeMap typeMap : list) {
            TypeMap map = new HashTypeMap();
            result.add(map);
            for (Map.Entry<String, Object> entry : typeMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                String name = camelCase(key);
                map.put(name, value);
            }
        }
        return result;
    }

    private String camelCase(String column) {
        String result = column.toLowerCase();
        Pattern pattern = Pattern.compile("(_)([a-z])");
        Matcher matcher = pattern.matcher(result);
        while(matcher.find()) {
            String group = matcher.group(2);
            String word = group.toUpperCase();
            result = result.replaceFirst("_" + group, word);
        }
        return result;
    }

}
