package com.maple86.jsplet.db;

import java.util.Arrays;
import java.util.Objects;

public class SqlData {

    private final String sql;

    private final Object[] params;

    public SqlData(String sql, Object[] params) {
        this.sql = sql;
        this.params = params;
    }

    public String getSql() {
        return sql;
    }

    public Object[] getParams() {
        return params;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SqlData sqlData = (SqlData) o;
        return Objects.equals(sql, sqlData.sql) &&
                Arrays.equals(params, sqlData.params);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(sql);
        result = 31 * result + Arrays.hashCode(params);
        return result;
    }

    @Override
    public String toString() {
        return "SqlData{" +
                "sql='" + sql + '\'' +
                ", params=" + Arrays.toString(params) +
                '}';
    }
}
