package com.maple86.jsplet.db;

import com.maple86.jsplet.annotation.Inquirer;
import com.maple86.jsplet.annotation.Param;
import com.maple86.jsplet.api.InquirerMarker;

import java.io.Serializable;

@Inquirer(PageInquirerHandler.class)
public class PageInfo implements InquirerMarker, Serializable {

    /**
     * 总页数
     */
    private int totalPage;

    /**
     * 数据库总记录数
     */
    private long totalCount;

    /**
     * 当前页数
     */
    @Param(value = "currentPage", required = false, explain = "当前页数")
    private int currentPage = 1;

    /**
     * 当前页数
     */
    private int start;

    /**
     * 每页记录数
     */
    @Param(value = "pageSize", required = false, explain = "每页记录数")
    private int pageSize = 10;

    public PageInfo() {
    }

    public void calculate(long totalCount) {
        if (pageSize < 1){
            pageSize = 10;
        }
        totalPage = (int) (totalCount / pageSize);
        if (totalCount % pageSize > 0){
            totalPage++;
        }
        if (currentPage > totalPage){
            currentPage = totalPage;
        }
        if (currentPage < 1){
            currentPage = 1;
        }
        start = (currentPage - 1) * pageSize;
        this.totalCount = totalCount;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getStart() {
        return start;
    }

    public int getPageSize() {
        return pageSize;
    }

    @Override
    public String toString() {
        return "PageInfo{" +
                "totalPage=" + totalPage +
                ", totalCount=" + totalCount +
                ", currentPage=" + currentPage +
                ", start=" + start +
                ", pageSize=" + pageSize +
                '}';
    }
}
