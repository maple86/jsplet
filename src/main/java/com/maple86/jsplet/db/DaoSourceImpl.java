package com.maple86.jsplet.db;

import com.maple86.jsplet.api.DaoSource;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.api.SqlSource;
import com.maple86.jsplet.exception.JspletDaoException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class DaoSourceImpl implements DaoSource {

    private final Log log = LogFactory.getLog(this.getClass());

    private final JspletPage jspletPage;

    private final String content;

    private Element root;

    public DaoSourceImpl(JspletPage jspletPage, String content) {
        this.jspletPage = jspletPage;
        this.content = content.trim();
    }

    @Override
    public JspletPage getJspletPage() {
        return jspletPage;
    }

    @Override
    public SqlSource getSqlSource(String id) {
        if ("".equals(content)) {
            throw new JspletDaoException("查询编号未定义：" + id);
        }
        if (null == root) {
            parseContent();
        }
        NodeList childNodes = root.getChildNodes();
        int length = childNodes.getLength();
        for(int i = 0; i < length; i++) {
            Node node = childNodes.item(i);
            if (!(node instanceof Element)) {
                continue;
            }
            Element element = (Element) node;
            String elementId = element.getAttribute("id");
            if (id.equals(elementId)) {
                return new SqlSourceImpl(element);
            }
        }
        throw new JspletDaoException("查询编号未定义：" + id);
    }

    private void parseContent() {
        try {
            log.debug("解析DaoSource数据");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            String head = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            StringReader reader = new StringReader(head + content);
            Document document = builder.parse(new InputSource(reader));
            root = document.getDocumentElement();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new JspletDaoException(ex);
        }
    }

}
