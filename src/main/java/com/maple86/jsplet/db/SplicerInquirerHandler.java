package com.maple86.jsplet.db;

import com.maple86.jsplet.api.*;
import com.maple86.jsplet.map.TypeMap;

import java.util.List;

public class SplicerInquirerHandler implements InquirerHandler<Splicer> {

    private JspletEngine jspletEngine;

    private String dataSource;

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @Override
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<TypeMap> select(Splicer inquirer, SqlData sqlData) {
        String sql = sqlData.getSql();
        Object[] params = sqlData.getParams();
        String content = inquirer.getContent();
        Object[] ps = inquirer.getParams();
        sql = sql + content;
        params = concatParams(params, ps);
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        DataSession dataSession = dataSessionFactory.openSession();
        return dataSession.select(dataSource, new SqlData(sql, params));
    }

    private static Object[] concatParams(Object[] params, Object[] ps) {
        if (null == ps || ps.length == 0) {
            return params;
        }
        Object[] rs = new Object[params.length + ps.length];
        System.arraycopy(params, 0, rs, 0, params.length);
        System.arraycopy(ps, 0, rs, params.length, ps.length);
        return rs;
    }

}
