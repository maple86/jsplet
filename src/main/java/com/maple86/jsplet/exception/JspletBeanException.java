package com.maple86.jsplet.exception;

public class JspletBeanException extends JspletException {

    public JspletBeanException() {
    }

    public JspletBeanException(String message) {
        super(message);
    }

    public JspletBeanException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletBeanException(Throwable cause) {
        super(cause);
    }

    public JspletBeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
