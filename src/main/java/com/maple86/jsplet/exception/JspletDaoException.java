package com.maple86.jsplet.exception;

public class JspletDaoException extends JspletException {

    public JspletDaoException() {
    }

    public JspletDaoException(String message) {
        super(message);
    }

    public JspletDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletDaoException(Throwable cause) {
        super(cause);
    }

    public JspletDaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
