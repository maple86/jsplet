package com.maple86.jsplet.exception;

public class JspletTargetException extends JspletException {

    private Throwable target;

    public JspletTargetException(Throwable cause) {
        super(cause);
        this.target = cause;
    }

    public Throwable getTargetException() {
        return target;
    }
}
