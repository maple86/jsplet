package com.maple86.jsplet.exception;

public class JspletPageLoadException extends JspletException {

    public JspletPageLoadException() {
    }

    public JspletPageLoadException(String message) {
        super(message);
    }

    public JspletPageLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletPageLoadException(Throwable cause) {
        super(cause);
    }

    public JspletPageLoadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
