package com.maple86.jsplet.exception;

public class JspletParameterException extends JspletException {

    public JspletParameterException() {
    }

    public JspletParameterException(String message) {
        super(message);
    }

    public JspletParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletParameterException(Throwable cause) {
        super(cause);
    }

    public JspletParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
