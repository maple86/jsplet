package com.maple86.jsplet.exception;

public class JspletStatusException extends JspletException {

    private final int status;

    public JspletStatusException(int status, String message) {
        super(message, null, false, false);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
