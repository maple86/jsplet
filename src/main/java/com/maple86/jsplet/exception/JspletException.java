package com.maple86.jsplet.exception;

public class JspletException extends RuntimeException {

    public JspletException() {
    }

    public JspletException(String message) {
        super(message);
    }

    public JspletException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletException(Throwable cause) {
        super(cause);
    }

    public JspletException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
