package com.maple86.jsplet.exception;

public class JspletConfigException extends JspletException {

    public JspletConfigException() {
    }

    public JspletConfigException(String message) {
        super(message);
    }

    public JspletConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    public JspletConfigException(Throwable cause) {
        super(cause);
    }

    public JspletConfigException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
