package com.maple86.jsplet.exception;

public class JspletEventException extends JspletException {

    private Throwable target;

    public JspletEventException(Throwable cause) {
        super(cause);
        this.target = cause;
    }

    public Throwable getTargetException() {
        return target;
    }

}
