package com.maple86.jsplet.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.core.ActionExecutorImpl;
import com.maple86.jsplet.core.JspletEngineImpl;
import com.maple86.jsplet.exception.JspletStatusException;
import com.maple86.jsplet.result.ErrorStatus;
import com.maple86.jsplet.result.ModelMap;
import com.maple86.jsplet.result.ModelView;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class DispatcherFilter implements Filter {

    private final Log log = LogFactory.getLog(this.getClass());

    private JspletEngine jspletEngine;

    private volatile boolean started = false;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init(FilterConfig filterConfig) {
        ServletContext servletContext = filterConfig.getServletContext();
        jspletEngine = new JspletEngineImpl(servletContext);
        jspletEngine.build();
        new Thread(() -> {
            try {
                Thread.sleep(1000);
                start();
            } catch (InterruptedException ex) {
                log.error("", ex);
            }
        }).start();
    }

    private void start() {
        if (!started) {
            synchronized (this) {
                if(!started) {
                    started = true;
                    try {
                        jspletEngine.init();
                    } catch (Exception ex) {
                        log.error("", ex);
                        throw ex;
                    }
                }
            }
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        start();
        request.setCharacterEncoding("UTF-8");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getRequestURI();
        path = jspletEngine.mappingPath(path);
        JspletModule jspletModule = matchModule(path);
        if (null == jspletModule) {
            response.sendError(404, path);
            return;
        }
        String contextPath = jspletModule.getContextPath();
        if (!"/".equals(contextPath)) {
            path = path.substring(contextPath.length());
        }
        if ("".equals(path)) {
            path = "/";
        }
        String moduleName = jspletModule.getName();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        if (matchActionSuffix(path)) {
            String actionSuffix = jspletSetting.getActionSuffix();
            path = path.substring(0, path.length() - actionSuffix.length());
            try {
                JspletPage jspletPage = jspletModule.getJspletPage("action", path);
                if (null == jspletPage) {
                    response.sendError(404, path);
                    return;
                }
                ActionExecutorImpl actionExecutor = new ActionExecutorImpl(jspletPage);
                Object result = actionExecutor.execute(request, response);
                handleResult(moduleName, request, response, result);
            } catch (Throwable throwable) {
                handleException(response, throwable);
            }
        } else {
            handleStatic(moduleName, path, request, response);
        }
    }

    private void handleException(HttpServletResponse response, Throwable throwable) throws ServletException, IOException {
        if (throwable instanceof JspletStatusException) {
            JspletStatusException ex = (JspletStatusException) throwable;
            int status = ex.getStatus();
            String message = ex.getMessage();
            response.sendError(status, message);
            return;
        }
        if (throwable instanceof ServletException) {
            throw (ServletException) throwable;
        } else if (throwable instanceof IOException) {
            throw (IOException) throwable;
        } else {
            throw new ServletException(throwable);
        }
    }

    private void handleResult(String module, HttpServletRequest request, HttpServletResponse response, Object result) throws IOException, ServletException {
        if (null == result) {
            return;
        }
        if (result instanceof ErrorStatus) {
            String error = ((ErrorStatus) result).getError();
            int status = ((ErrorStatus) result).getStatus();
            if (null != error) {
                response.sendError(status, error);
            } else {
                response.sendError(status);
            }
            return;
        }
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        if (result instanceof String) {
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.write((String) result);
            out.flush();
            out.close();
        } else if (result instanceof ModelView) {
            ModelView modelView = (ModelView) result;
            String view = modelView.getView();
            ModelMap model = modelView.getModel();
            if (view.startsWith("redirect:")) {
                String location = parseLocation(request, modelView);
                response.sendRedirect(location);
            } else {
                String viewPath = "/" + module + "/view" + view + jspletSetting.getViewSuffix();
                if (null != model) {
                    model.forEach(request::setAttribute);
                }
                request.getRequestDispatcher(viewPath).forward(request, response);
            }
        } else {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            objectMapper.writeValue(out, result);
            out.flush();
            out.close();
        }
    }

    private String parseLocation(HttpServletRequest request, ModelView modelView) throws UnsupportedEncodingException {
        String view = modelView.getView();
        ModelMap model = modelView.getModel();
        String cp = request.getContextPath();
        String loc = view.substring(9);
        if (!loc.startsWith("http://") && !loc.startsWith("https://")) {
            loc = cp + loc;
        }
        boolean flag = loc.contains("?");
        StringBuilder buf = new StringBuilder();
        for (Map.Entry<String, Object> entry : model.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (flag) {
                buf.append("&");
            } else {
                buf.append("?");
                flag = true;
            }
            buf.append(key);
            buf.append("=");
            buf.append(URLEncoder.encode(value + "", "UTF-8"));
        }
        return loc + buf;
    }

    private void handleStatic(String module, String path, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String basePath = "/" +  module + "/static";
        response = new HttpServletResponseWrapper(response) {
            @Override
            public void sendError(int sc, String msg) throws IOException {
                if (404 == sc) {
                    super.sendError(sc, path);
                } else {
                    super.sendError(sc, msg);
                }
            }
        };
        jspletEngine.getServletContext().getRequestDispatcher(basePath + path).forward(request, response);
    }

    private boolean matchActionSuffix(String path) {
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        String actionSuffix = jspletSetting.getActionSuffix();
        if ("".equals(actionSuffix)) {
            return !path.contains(".");
        }
        return path.endsWith(actionSuffix);
    }

    private JspletModule matchModule(String path) {
        String tempPath = path + "/";
        JspletModule rootModule = null;
        List<String> moduleNames = jspletEngine.getModuleNames();
        for (String moduleName : moduleNames) {
            JspletModule jspletModule = jspletEngine.getModule(moduleName);
            String contextPath = jspletModule.getContextPath();
            if ("/".equals(contextPath)) {
                rootModule = jspletModule;
            } else {
                if (tempPath.startsWith(contextPath + "/")) {
                    return jspletModule;
                }
            }
        }
        return rootModule;
    }

    @Override
    public void destroy() {
        jspletEngine.destroy();
    }
}
