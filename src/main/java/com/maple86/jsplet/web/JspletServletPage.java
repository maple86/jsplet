package com.maple86.jsplet.web;

import com.maple86.jsplet.api.*;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.DaoExecutorImpl;
import com.maple86.jsplet.db.TransactionLevel;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.HttpJspPage;
import java.io.IOException;
import java.util.concurrent.Callable;

public class JspletServletPage implements HttpJspPage {

    private ServletConfig config;

    private JspletPage jspletPage;

    @Override
    public final void init(ServletConfig config) {
        this.config = config;
    }

    @Override
    public ServletConfig getServletConfig() {
        return config;
    }

    public ServletContext getServletContext() {
        return config.getServletContext();
    }

    public JspletModule getJspletModule() {
        return jspletPage.getJspletModule();
    }

    @Override
    public final void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String method = request.getMethod();
        if ("GET".equalsIgnoreCase(method)) {
            // 该方法扫描页面定义的方法和dao等
            _jspService(request, response);
            req.setAttribute("page", this);
            jspletPage = (JspletPage) req.getAttribute("jspletPage");
        } else if ("HEAD".equalsIgnoreCase(method)) {
            // 通过该方法抛异常，来定位异常页面
            Throwable exception = (Throwable) req.getAttribute("exception");
            if (null != exception) {
                if (exception instanceof ServletException) {
                    throw (ServletException) exception;
                } else if (exception instanceof IOException) {
                    throw (IOException) exception;
                } else if (exception instanceof RuntimeException) {
                    throw (RuntimeException) exception;
                } else {
                    throw new ServletException(exception);
                }
            }
        }
    }

    public DaoExecutor dao(String id) {
        DaoSource daoSource = jspletPage.getDaoSource();
        JspletModule jspletModule = jspletPage.getJspletModule();
        SqlSource sqlSource = daoSource.getSqlSource(id);
        return new DaoExecutorImpl(jspletModule, sqlSource);
    }

    public <T> T transact(Callable<T> callable, TransactionLevel level) throws Exception {
        JspletModule jspletModule = jspletPage.getJspletModule();
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        DataSession session = dataSessionFactory.openSession();
        if (session.hasTransaction()) {
            // 如果已经开启了事务，则不处理事务，交由外层处理
            return callable.call();
        } else {
            // 如果没有开启事务，则开启并处理事务
            session.openTransaction(level);
            try {
                T result = callable.call();
                session.commit();
                return result;
            } catch (Exception ex) {
                session.rollback();
                throw ex;
            }
        }
    }

    public <T> T transact(Callable<T> callable) throws Exception {
        JspletModule jspletModule = jspletPage.getJspletModule();
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        TransactionLevel transactionLevel = jspletSetting.getDefaultTransactionLevel();
        return transact(callable, transactionLevel);
    }

    @Override
    public String getServletInfo() {
        return "";
    }

    @Override
    public final void destroy() {
    }

    @Override
    public void jspInit() {
    }

    @Override
    public void jspDestroy() {
    }

    @Override
    public void _jspService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
