package com.maple86.jsplet.result;

import com.maple86.jsplet.util.BeanUtil;

import java.util.Map;

public class ModelView {

    private String view;

    private ModelMap model;

    public ModelView(String view) {
        this.view = view;
    }

    public ModelView(String view, ModelMap model) {
        this.view = view;
        this.model = model;
    }

    public String getView() {
        return view;
    }

    public ModelMap getModel() {
        return model;
    }

    public ModelView put(String name, Object value) {
        if (null == model) {
            model = new ModelMap();
        }
        model.put(name, value);
        return this;
    }

    public ModelView putAll(Object model) {
        Map<String, Object> map = BeanUtil.parseMap(model);
        if (null == this.model) {
            this.model = new ModelMap();
        }
        this.model.putAll(map);
        return this;
    }

}
