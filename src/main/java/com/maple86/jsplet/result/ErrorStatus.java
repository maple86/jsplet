package com.maple86.jsplet.result;

public class ErrorStatus {

    private int status;

    private String error;

    public ErrorStatus(int status) {
        this.status = status;
    }

    public ErrorStatus(int status, String error) {
        this.status = status;
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

}
