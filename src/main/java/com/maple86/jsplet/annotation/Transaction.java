package com.maple86.jsplet.annotation;

import com.maple86.jsplet.db.TransactionLevel;
import com.maple86.jsplet.weaver.TransactionWeaver;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(METHOD)
@Retention(RUNTIME)
@Documented
@Weaver(TransactionWeaver.class)
public @interface Transaction {

    TransactionLevel level() default TransactionLevel.TRANSACTION_DEFAULT;

}
