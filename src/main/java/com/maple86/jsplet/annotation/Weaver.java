package com.maple86.jsplet.annotation;

import com.maple86.jsplet.weaver.WeaverHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(ElementType.ANNOTATION_TYPE)
@Retention(RUNTIME)
@Documented
public @interface Weaver {

    Class<? extends WeaverHandler> value();

}
