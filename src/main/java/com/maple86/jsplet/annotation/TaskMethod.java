package com.maple86.jsplet.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface TaskMethod {

    String cron() default "";

    long delay() default -1L;

    long interval() default -1L;

    int repeat() default 0;

}
