package com.maple86.jsplet.annotation;

import com.maple86.jsplet.weaver.ResponseViewWeaver;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(METHOD)
@Retention(RUNTIME)
@Documented
@Weaver(ResponseViewWeaver.class)
public @interface ResponseView {
}
