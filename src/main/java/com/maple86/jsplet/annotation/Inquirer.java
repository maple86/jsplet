package com.maple86.jsplet.annotation;

import com.maple86.jsplet.api.InquirerHandler;
import com.maple86.jsplet.api.InquirerMarker;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface Inquirer {

    Class<? extends InquirerHandler<? extends InquirerMarker>> value();

}
