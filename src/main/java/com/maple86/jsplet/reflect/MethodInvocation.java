package com.maple86.jsplet.reflect;

import java.lang.reflect.Method;

public class MethodInvocation {

    private MethodInvoker methodInvoker;

    private Object target;

    private Method method;

    private Object[] arguments;

    public MethodInvocation(MethodInvoker methodInvoker, Object target, Method method, Object[] arguments) {
        this.methodInvoker = methodInvoker;
        this.target = target;
        this.method = method;
        this.arguments = arguments;
    }

    public Object getTarget() {
        return target;
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public Object invoke() throws Throwable {
        return methodInvoker.invoke(target, method, arguments);
    }
}
