package com.maple86.jsplet.reflect;

import java.lang.reflect.Method;

public interface MethodInvoker {

    Object invoke(Object target, Method method, Object... arguments) throws Throwable;

}
