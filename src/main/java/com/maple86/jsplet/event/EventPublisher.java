package com.maple86.jsplet.event;

import com.maple86.jsplet.exception.JspletEventException;

public interface EventPublisher {

    void publishEvent(Object event) throws JspletEventException;

    void publishEvent(Object event, EventExceptionHandler handler) throws JspletEventException;

}
