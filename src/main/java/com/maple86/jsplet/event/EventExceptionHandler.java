package com.maple86.jsplet.event;

public interface EventExceptionHandler {

    void handle(Throwable exception) throws Throwable;

}
