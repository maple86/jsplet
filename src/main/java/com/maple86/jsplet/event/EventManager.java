package com.maple86.jsplet.event;

import com.maple86.jsplet.annotation.EventListener;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletEngineAware;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.exception.JspletEventException;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventManager implements EventPublisher, JspletEngineAware {

    private JspletEngine jspletEngine;

    private final List<JspletPage> jspletPages = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @PostConstruct
    public void init() {
        List<String> moduleNames = jspletEngine.getModuleNames();
        for (String moduleName : moduleNames) {
            JspletModule jspletModule = jspletEngine.getModule(moduleName);
            List<JspletPage> pages = jspletModule.getJspletPages("event");
            jspletPages.addAll(pages);
        }
    }

    @Override
    public void publishEvent(Object event) throws JspletEventException {
        publishEvent(event, (exception) -> {
            throw exception;
        });
    }

    @Override
    public void publishEvent(Object event, EventExceptionHandler handler) throws JspletEventException {
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        for (JspletPage jspletPage : jspletPages) {
            if (jspletSetting.isDetectPageChange()) {
                jspletPage.detectChange();
            }
            Object target = jspletPage.getTarget();
            if (null == target) {
                continue;
            }
            Method[] methods = target.getClass().getMethods();
            for (Method method : methods) {
                EventListener annotation = method.getAnnotation(EventListener.class);
                if (null == annotation) {
                    continue;
                }
                if (matchEvent(method, event)) {
                    executeEvent(target, method, event, handler);
                }
            }
        }
    }

    private boolean matchEvent(Method method, Object event) {
        Class<?>[] types = method.getParameterTypes();
        if (types.length == 0) {
            return true;
        }
        return types[0].isInstance(event);
    }

    private void executeEvent(Object target, Method method, Object event, EventExceptionHandler handler) throws JspletEventException {
        int count = method.getParameterCount();
        Object[] args = new Object[count];
        args[0] = event;
        try {
            method.invoke(target, args);
        } catch (IllegalAccessException ex) {
            handleException(handler, ex);
        } catch (InvocationTargetException ex) {
            Throwable exception = ex.getTargetException();
            handleException(handler, exception);
        }
    }

    private void handleException(EventExceptionHandler handler, Throwable exception) throws JspletEventException {
        try {
            handler.handle(exception);
        } catch (Throwable ex) {
            throw new JspletEventException(ex);
        }
    }

}
