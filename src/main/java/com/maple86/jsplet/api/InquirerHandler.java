package com.maple86.jsplet.api;

import com.maple86.jsplet.db.SqlData;
import com.maple86.jsplet.map.TypeMap;

import java.util.List;

public interface InquirerHandler<E extends InquirerMarker> {

    void setJspletEngine(JspletEngine jspletEngine);

    void setDataSource(String dataSource);

    List<TypeMap> select(E inquirer, SqlData sqlData);

}
