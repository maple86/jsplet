package com.maple86.jsplet.api;

import com.maple86.jsplet.config.JspletSetting;
import org.apache.commons.fileupload.FileItemFactory;

import javax.servlet.ServletContext;
import java.util.List;

public interface JspletEngine {

    void build();

    void init();

    ServletContext getServletContext();

    DataSessionFactory getDataSessionFactory();

    List<String> getModuleNames();

    JspletModule getModule(String name);

    JspletSetting getJspletSetting();

    FileItemFactory getFileItemFactory();

    List<String> getBeanNames();

    Object getBean(String name);

    <T> T getBean(Class<T> type);

    <T> List<T> getBeans(Class<T> type);

    String mappingPath(String path);

    void destroy();

}
