package com.maple86.jsplet.api;

import com.maple86.jsplet.db.SqlData;
import com.maple86.jsplet.db.TransactionLevel;
import com.maple86.jsplet.map.TypeMap;

import java.sql.DatabaseMetaData;
import java.util.List;

public interface DataSession {

    boolean hasTransaction();

    void openTransaction(TransactionLevel level);

    void close();

    void commit();

    void rollback();

    boolean isClosed();

    List<TypeMap> insert(String dataSource, SqlData sqlData);

    int delete(String dataSource, SqlData sqlData);

    int update(String dataSource, SqlData sqlData);

    List<TypeMap>  select(String dataSource, SqlData sqlData);

    DatabaseMetaData getDatabaseMetaData(String dataSource);

}
