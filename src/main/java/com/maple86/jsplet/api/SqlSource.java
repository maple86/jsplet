package com.maple86.jsplet.api;

import com.maple86.jsplet.db.SqlData;

import java.util.Map;

public interface SqlSource {

    SqlData parseSql(Map<String, Object> paramMap);

}
