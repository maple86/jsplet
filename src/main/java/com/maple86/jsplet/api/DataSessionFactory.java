package com.maple86.jsplet.api;

public interface DataSessionFactory {

    boolean hasSession();

    DataSession openSession();

    void closeSession();

}
