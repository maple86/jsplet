package com.maple86.jsplet.api;

import com.maple86.jsplet.exception.JspletPageLoadException;

import java.lang.reflect.Method;

public interface JspletPage {

    void init() throws JspletPageLoadException;

    JspletModule getJspletModule();

    String getType();

    String getPath();

    String getPagePath();

    Object getTarget();

    DaoSource getDaoSource();

    int getVersion();

    void detectChange() throws JspletPageLoadException;

    Object invoke(Method method, Object... arguments) throws Throwable;

    void destroy();
}
