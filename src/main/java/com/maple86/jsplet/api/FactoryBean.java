package com.maple86.jsplet.api;

public interface FactoryBean<T> {

    T getObject();

    Class<T> getObjectType();

    boolean isSingleton();

}
