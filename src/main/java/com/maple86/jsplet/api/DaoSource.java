package com.maple86.jsplet.api;

public interface DaoSource {

    JspletPage getJspletPage();

    SqlSource getSqlSource(String id);

}
