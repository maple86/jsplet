package com.maple86.jsplet.api;

import java.util.List;

public interface BeanFactory {

    <T> T getBean(Class<T> type);

    <T> List<T> getBeans(Class<T> type);

}
