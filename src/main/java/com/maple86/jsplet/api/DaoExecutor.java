package com.maple86.jsplet.api;

import com.maple86.jsplet.map.TypeMap;

import java.util.List;

public interface DaoExecutor {

    DaoExecutor put(String name, Object value);

    DaoExecutor putAll(Object model);

    List<TypeMap> select();

    List<TypeMap> select(InquirerMarker inquirer);

    <T> List<T> select(Class<T> type);

    <T> List<T> select(Class<T> type, InquirerMarker inquirer);

    TypeMap selectOne(InquirerMarker inquirer);

    TypeMap selectOne();

    <T> T selectOne(Class<T> type);

    <T> T selectOne(Class<T> type, InquirerMarker inquirer);

    Object selectValue();

    Object selectValue(InquirerMarker inquirer);

    <T> T selectValue(Class<T> type);

    <T> T selectValue(Class<T> type, InquirerMarker inquirer);

    List<TypeMap> insert();

    TypeMap insertOne();

    int update();

    int delete();

}
