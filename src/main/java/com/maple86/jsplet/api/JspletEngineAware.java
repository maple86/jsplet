package com.maple86.jsplet.api;

public interface JspletEngineAware {

    void setJspletEngine(JspletEngine jspletEngine);

}
