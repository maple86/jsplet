package com.maple86.jsplet.api;

import java.util.List;

public interface JspletModule {

    void init();

    JspletEngine getJspletEngine();

    String getName();

    String getContextPath();

    List<JspletPage> getJspletPages(String type);

    JspletPage getJspletPage(String type, String path);

    String getDataSource();

    void destroy();

}
