package com.maple86.jsplet.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ActionExecutor {

    Object execute(HttpServletRequest request, HttpServletResponse response) throws Throwable;

}
