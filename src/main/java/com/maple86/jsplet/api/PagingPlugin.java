package com.maple86.jsplet.api;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

public interface PagingPlugin {

    boolean accept(DatabaseMetaData metaData) throws SQLException;

    String parseSql(String sql, int offset, int limit);

}
