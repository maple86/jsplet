package com.maple86.jsplet.core;

import com.maple86.jsplet.annotation.ServiceMapper;
import com.maple86.jsplet.api.BeanFactory;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletEngineAware;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.exception.JspletBeanException;

import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.List;

public class ServiceBeanFactory implements BeanFactory, JspletEngineAware {

    private JspletEngine jspletEngine;

    @Override
    public void setJspletEngine(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getBean(Class<T> type) {
        ServiceMapper annotation = type.getAnnotation(ServiceMapper.class);
        if (null == annotation) {
            return null;
        }
        String module = annotation.module();
        JspletModule jspletModule = jspletEngine.getModule(module);
        if (null == jspletModule) {
            throw new JspletBeanException("不存在模块：" + module);
        }
        ServiceHandler handler = new ServiceHandler(jspletModule);
        return (T) Proxy.newProxyInstance(type.getClassLoader(), new Class[] { type }, handler);
    }

    @Override
    public <T> List<T> getBeans(Class<T> type) {
        T bean = getBean(type);
        if (null != bean) {
            return Collections.singletonList(bean);
        }
        return Collections.emptyList();
    }

}
