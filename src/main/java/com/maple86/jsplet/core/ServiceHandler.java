package com.maple86.jsplet.core;

import com.maple86.jsplet.annotation.PagePath;
import com.maple86.jsplet.annotation.ServiceMethod;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.db.CloseSessionInvoker;
import com.maple86.jsplet.exception.JspletException;
import com.maple86.jsplet.weaver.ComponentWeaverInvoker;
import com.maple86.jsplet.weaver.WeaverInvoker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ServiceHandler implements InvocationHandler {

    private final Log log = LogFactory.getLog(this.getClass());

    private final JspletModule jspletModule;

    public ServiceHandler(JspletModule jspletModule) {
        this.jspletModule = jspletModule;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        PagePath pagePath = method.getAnnotation(PagePath.class);
        if (null == pagePath) {
            throw new JspletException("缺少注解：" + PagePath.class.getName());
        }
        String fullPath = pagePath.value();
        String[] strings = fullPath.split("#");
        String path = strings[0];
        String methodName = "default";
        if (strings.length > 1) {
            methodName = strings[1];
        }
        JspletPage jspletPage = jspletModule.getJspletPage("service", path);
        Object target = jspletPage.getTarget();
        Method pageMethod = getPageMethod(target, methodName);
        if (null == pageMethod) {
            throw new JspletException("页面：" + jspletPage.getPagePath() + "不存在方法：" + methodName);
        }
        log.trace("开始执行，页面：" + jspletPage.getPagePath() + "，方法：" + pageMethod.getName());
        if (log.isDebugEnabled()) {
            log.trace("执行参数：" + Arrays.toString(args));
        }
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        WeaverInvoker weaverInvoker = new ComponentWeaverInvoker(jspletPage, (tar, me, as) -> jspletPage.invoke(me, as));
        CloseSessionInvoker closeSessionInvoker = new CloseSessionInvoker(jspletEngine, weaverInvoker);
        Object result = closeSessionInvoker.invoke(target, pageMethod, args);
        log.trace("结束执行，页面：" + jspletPage.getPagePath() + "，方法：" + pageMethod.getName());
        if (log.isDebugEnabled()) {
            log.trace("执行结果：" + result);
        }
        return result;
    }

    private Method getPageMethod(Object target, String name) {
        Method[] methods = target.getClass().getMethods();
        for (Method method : methods) {
            ServiceMethod annotation = method.getAnnotation(ServiceMethod.class);
            if (null == annotation) {
                continue;
            }
            if (annotation.value().equals(name)) {
                return method;
            }
        }
        return null;
    }

}
