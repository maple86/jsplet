package com.maple86.jsplet.core;

import com.maple86.jsplet.api.DataSessionFactory;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.DataSessionFactoryImpl;
import com.maple86.jsplet.db.SimplePagingPlugin;
import com.maple86.jsplet.exception.JspletConfigException;
import com.maple86.jsplet.util.ElementUtil;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JspletEngineImpl implements JspletEngine {

    private static class PathMapping {

        private String regex;

        private String path;

    }

    private final ServletContext servletContext;

    private final BeanManager beanManager = new BeanManager(this);

    private final DataSessionFactory dataSessionFactory = new DataSessionFactoryImpl(this);

    private final Map<String, JspletModule> moduleMap = new HashMap<>();

    private final JspletSetting jspletSetting = new JspletSetting();

    private FileItemFactory fileItemFactory;

    private final List<PathMapping> pathMappings = new ArrayList<>();

    public JspletEngineImpl(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public DataSessionFactory getDataSessionFactory() {
        return dataSessionFactory;
    }

    @Override
    public List<String> getModuleNames() {
        return new ArrayList<>(moduleMap.keySet());
    }


    @Override
    public JspletModule getModule(String name) {
        return moduleMap.get(name);
    }

    @Override
    public JspletSetting getJspletSetting() {
        return jspletSetting;
    }

    @Override
    public FileItemFactory getFileItemFactory() {
        return fileItemFactory;
    }

    @Override
    public List<String> getBeanNames() {
        return beanManager.getBeanNames();
    }

    @Override
    public Object getBean(String name) {
        return beanManager.getBean(name);
    }

    @Override
    public <T> T getBean(Class<T> type) {
        return beanManager.getBean(type);
    }

    @Override
    public <T> List<T> getBeans(Class<T> type) {
        return beanManager.getBeans(type);
    }

    @Override
    public void build() {
        Element element = loadConfig();
        Element settings = ElementUtil.getElement(element, "settings");
        if (null != settings) {
            jspletSetting.load(settings);
        }
        Element beans = ElementUtil.getElement(element, "beans");
        if (null != beans) {
            loadBeans(beans);
        }
        Element mappings = ElementUtil.getElement(element, "mappings");
        if (null != mappings) {
            loadMappings(mappings);
        }
        Element modules = ElementUtil.getElement(element, "modules");
        if (null != modules) {
            loadModules(modules);
        }
        int uploadMemoryThreshold = jspletSetting.getUploadMemoryThreshold();
        File repository = new File(System.getProperty("java.io.tmpdir"));
        fileItemFactory = new DiskFileItemFactory(uploadMemoryThreshold, repository);
        beanManager.build();
    }

    @Override
    public String mappingPath(String path) {
        for (PathMapping pathMapping : pathMappings) {
            String regex = pathMapping.regex;
            if (path.matches(regex)) {
                return pathMapping.path;
            }
        }
        return path;
    }

    private Element loadConfig() {
        try (InputStream input = getConfigInput()) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(input);
            return document.getDocumentElement();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            throw new JspletConfigException(ex);
        }
    }

    private InputStream getConfigInput() {
        InputStream input = JspletEngineImpl.class.getResourceAsStream("/jsplet-test.xml");
        if (null != input) {
            return input;
        }
        input = JspletEngineImpl.class.getResourceAsStream("/jsplet.xml");
        if (null != input) {
            return input;
        }
        throw new JspletConfigException("未找到配置文件！");
    }

    private void loadBeans(Element parent) {
        List<Element> elements = ElementUtil.getElements(parent, "bean");
        for (Element element : elements) {
            beanManager.register(element);
        }
        beanManager.register(SimplePagingPlugin.class);
        beanManager.register(ServiceBeanFactory.class);
    }

    private void loadMappings(Element parent) {
        List<Element> elements = ElementUtil.getElements(parent, "mapping");
        for (Element element : elements) {
            String regex = element.getAttribute("regex");
            String path = element.getAttribute("path");
            PathMapping pathMapping = new PathMapping();
            pathMapping.regex = regex;
            pathMapping.path = path;
            pathMappings.add(pathMapping);
        }
    }

    private void loadModules(Element parent) {
        List<Element> elements = ElementUtil.getElements(parent, "module");
        for (Element element : elements) {
            String name = element.getAttribute("name");
            String dataSource = element.getAttribute("dataSource");
            String contextPath = element.getAttribute("contextPath");
            JspletModule jspletModule = new JspletModuleImpl(this, name, contextPath, dataSource);
            moduleMap.put(name, jspletModule);
        }
    }

    @Override
    public void init() {
        moduleMap.forEach((name, module) -> module.init());
        beanManager.init();
    }

    @Override
    public void destroy() {
        moduleMap.forEach((name, module) -> module.destroy());
        beanManager.destroy();
    }

}
