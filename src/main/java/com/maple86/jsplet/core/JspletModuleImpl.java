package com.maple86.jsplet.core;

import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class JspletModuleImpl implements JspletModule {

    private final JspletEngine jspletEngine;

    private final String name;

    private final String contextPath;

    private final String dataSource;

    private final Map<String, Map<String, JspletPage>> pageMap = new ConcurrentHashMap<>();

    public JspletModuleImpl(JspletEngine jspletEngine, String name, String contextPath, String dataSource) {
        this.jspletEngine = jspletEngine;
        this.name = name;
        this.contextPath = contextPath;
        this.dataSource = dataSource;
    }

    @Override
    public void init() {
    }

    @Override
    public JspletEngine getJspletEngine() {
        return jspletEngine;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getContextPath() {
        return contextPath;
    }

    @Override
    public List<JspletPage> getJspletPages(String type) {
        List<String> paths = findPaths(type);
        List<JspletPage> list = new ArrayList<>(paths.size());
        for (String path : paths) {
            JspletPage jspletPage = getJspletPage(type, path);
            list.add(jspletPage);
        }
        return list;
    }

    @Override
    public JspletPage getJspletPage(String type, String path) {
        Map<String, JspletPage> map = pageMap.get(type);
        if (null == map) {
            synchronized (this) {
                map = pageMap.get(type);
                if (null == map) {
                    map = new ConcurrentHashMap<>();
                    pageMap.put(type, map);
                }
            }
        }
        JspletPage jspletPage = map.get(path);
        if (null == jspletPage) {
            synchronized (this) {
                jspletPage = map.get(path);
                if (null == jspletPage) {
                    jspletPage = loadPage(type, path);
                }
            }
        }
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        if (null != jspletPage && jspletSetting.isDetectPageChange()) {
            jspletPage.detectChange();
            if (jspletPage.getTarget() == null) {
                synchronized (this) {
                    map.remove(path);
                }
                return null;
            }
        }
        return jspletPage;
    }

    private JspletPage loadPage(String type, String path) {
        JspletPage jspletPage = new JspletPageImpl(this, path, type);
        jspletPage.init();
        if (jspletPage.getTarget() == null) {
            return null;
        }
        return jspletPage;
    }

    private List<String> findPaths(String type) {
        ServletContext servletContext = jspletEngine.getServletContext();
        String path = servletContext.getRealPath("/" + name + "/" + type);
        List<String> list = new ArrayList<>();
        findPaths(list, new File(path), "");
        return list;
    }

    private void findPaths(List<String> list, File directory, String parent) {
        if (!directory.exists()) {
            return;
        }
        File[] files = directory.listFiles();
        if (null == files) {
            return;
        }
        for (File file : files) {
            String name = file.getName();
            String path = parent + "/" + name;
            if (file.isDirectory()) {
                findPaths(list, file, path);
            } else {
                if (!path.endsWith(".jsp")) {
                    continue;
                }
                path = path.substring(0, path.length() - 4);
                list.add(path);
            }
        }
    }

    @Override
    public String getDataSource() {
        return dataSource;
    }

    @Override
    public void destroy() {
        pageMap.values().forEach(map -> map.values().forEach(JspletPage::destroy));
    }

}
