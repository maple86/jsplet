package com.maple86.jsplet.core;

import com.maple86.jsplet.annotation.Autowired;
import com.maple86.jsplet.api.DaoSource;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.DaoSourceImpl;
import com.maple86.jsplet.exception.*;
import com.maple86.jsplet.web.JspletHttpServletRequest;
import com.maple86.jsplet.web.JspletHttpServletResponse;
import com.maple86.jsplet.web.NullWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.jsp.JspPage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class JspletPageImpl implements JspletPage {

    private final Log log = LogFactory.getLog(this.getClass());

    private final JspletModule jspletModule;

    private final String path;

    private final String type;

    private final String pagePath;

    private final File file;

    private volatile JspPage target;

    private DaoSource daoSource;

    private long time;

    private int version;

    public JspletPageImpl(JspletModule jspletModule, String path, String type) {
        this.jspletModule = jspletModule;
        this.path = path;
        this.type = type;
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        ServletContext servletContext = jspletEngine.getServletContext();
        String moduleName = jspletModule.getName();
        pagePath = "/" + moduleName + "/" + type + path + ".jsp";
        String realPath = servletContext.getRealPath(pagePath);
        file = new File(realPath);
    }

    @Override
    public void init() throws JspletPageLoadException {
        if (file.exists()) {
            load();
        }
    }

    @Override
    public JspletModule getJspletModule() {
        return jspletModule;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getPagePath() {
        return pagePath;
    }

    @Override
    public Object getTarget() {
        return target;
    }

    @Override
    public DaoSource getDaoSource() {
        return daoSource;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void detectChange() throws JspletPageLoadException {
        if (!file.exists()) {
            if (null != target) {
                target.jspDestroy();
                target = null;
            }
            return;
        }
        if (file.lastModified() > time) {
            synchronized (this) {
                if (file.lastModified() > time) {
                    load();
                }
            }
        }
    }

    private void load() throws JspletPageLoadException {
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        ServletContext servletContext = jspletEngine.getServletContext();
        JspletHttpServletRequest request = new JspletHttpServletRequest(servletContext, pagePath, "GET");
        request.setAttribute("jspletPage", this);
        StringWriter writer = new StringWriter();
        JspletHttpServletResponse response = new JspletHttpServletResponse(writer);
        try {
            request.getRequestDispatcher(pagePath).forward(request, response);
        } catch (ServletException ex) {
            if (jspletSetting.isPrintException()) {
                log.error("\r\n" + ex.getMessage());
            }
            throw new JspletPageLoadException(ex);
        } catch (IOException ex) {
            if (jspletSetting.isPrintException()) {
                log.error("", ex);
            }
            throw new JspletPageLoadException(ex);
        }
        int status = response.getStatus();
        String content = writer.toString();
        if (status >= 300) {
            String msg = content;
            if ("".equals(msg)) {
                msg = "页面错误！";
            }
            throw new JspletPageLoadException(msg);
        }
        Object page = request.getAttribute("page");
        if (null == page) {
            throw new JspletPageLoadException("页面类型错误！");
        }
        if (!(page instanceof JspPage)) {
            throw new JspletPageLoadException("页面类型错误！");
        }
        if (null != target) {
            target.jspDestroy();
        }
        wriedPage(page);
        daoSource = new DaoSourceImpl(this, content);
        target = (JspPage) page;
        time = System.currentTimeMillis();
        version++;
        target.jspInit();
    }

    private void wriedPage(Object page) {
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        try {
            Class<?> type = page.getClass();
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                Autowired autowired = field.getAnnotation(Autowired.class);
                if (null == autowired) {
                    continue;
                }
                boolean required = autowired.required();
                field.setAccessible(true);
                String name = autowired.name();
                if (!"".equals(name)) {
                    Object bean = jspletEngine.getBean(name);
                    field.set(page, bean);
                } else {
                    Class<?> fieldType = field.getType();
                    if (fieldType.isArray()) {
                        List<?> beans = jspletEngine.getBeans(fieldType);
                        Object array = Array.newInstance(fieldType.getComponentType(), beans.size());
                        for(int i = 0; i < beans.size(); i++) {
                            Array.set(array, i, beans.get(i));
                        }
                        field.set(page, array);
                    } else if (fieldType.equals(List.class)) {
                        List<?> beans = jspletEngine.getBeans(fieldType);
                        field.set(page, beans);
                    } else {
                        Object bean = jspletEngine.getBean(fieldType);
                        if (null == bean && required) {
                            throw new JspletBeanException("自动装配失败，未发现：" + fieldType.getName());
                        }
                        field.set(page, bean);
                    }
                }
            }
        } catch (IllegalAccessException ex) {
            throw new JspletPageLoadException(ex);
        }
    }

    @Override
    public Object invoke(Method method, Object[] arguments) throws Throwable {
        try {
            return method.invoke(target, arguments);
        } catch (InvocationTargetException ex) {
            Throwable throwable = ex.getTargetException();
            if (throwable instanceof JspletTargetException) {
                throwable = ((JspletTargetException) throwable).getTargetException();
            }
            printDetail(throwable);
            throw throwable;
        }
    }

    private void printDetail(Throwable throwable) {
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        if (!jspletSetting.isPrintException()) {
            return;
        }
        ServletContext servletContext = jspletEngine.getServletContext();
        JspletHttpServletRequest request = new JspletHttpServletRequest(servletContext, pagePath, "HEAD");
        request.setAttribute("exception", throwable);
        JspletHttpServletResponse response = new JspletHttpServletResponse(new NullWriter());
        try {
            request.getRequestDispatcher(pagePath).forward(request, response);
        } catch (ServletException ex) {
            log.error("\r\n" + ex.getMessage());
        } catch (Throwable ex) {
            log.error("", ex);
        }
    }

    @Override
    public void destroy() {
        try {
            target.jspDestroy();
        } catch (Throwable ex) {
            log.error("", ex);
        }
    }

}
