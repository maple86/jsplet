package com.maple86.jsplet.core;

import com.maple86.jsplet.annotation.Autowired;
import com.maple86.jsplet.api.FactoryBean;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletEngineAware;
import com.maple86.jsplet.exception.JspletBeanException;
import com.maple86.jsplet.exception.JspletConfigException;
import com.maple86.jsplet.exception.JspletException;
import com.maple86.jsplet.exception.JspletPageLoadException;
import com.maple86.jsplet.util.BeanUtil;
import com.maple86.jsplet.util.ElementUtil;
import org.w3c.dom.Element;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeanElement {

    private final BeanManager beanManager;

    private final String name;

    private final Object target;

    private String initMethod;

    private String destroyMethod;

    private volatile Object instance;

    private Element element;

    public BeanElement(BeanManager beanManager, Element element) {
        this.beanManager = beanManager;
        this.element = element;
        this.name = element.getAttribute("name");
        this.initMethod = element.getAttribute("init-method");
        this.destroyMethod = element.getAttribute("destroy-method");
        String className = element.getAttribute("class");
        try {
            Class<?> type = Class.forName(className);
            target = type.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new JspletConfigException(ex);
        }
    }

    public BeanElement(BeanManager beanManager, String name, Object target) {
        this.beanManager = beanManager;
        this.name = name;
        this.target = target;
    }

    public String getName() {
        return name;
    }

    public Object getTarget() {
        return target;
    }

    public void build() {
        if (null != element) {
            List<Element> list = ElementUtil.getElements(element);
            for (Element ele : list) {
                String name = ele.getAttribute("name");
                Object value = loadProperty(ele);
                BeanUtil.setProperty(target, name, value);
            }
        }
        if (target instanceof JspletEngineAware) {
            JspletEngine jspletEngine = beanManager.getJspletEngine();
            ((JspletEngineAware) target).setJspletEngine(jspletEngine);
        }
        try {
            Class<?> type = target.getClass();
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                Autowired autowired = field.getAnnotation(Autowired.class);
                if (null == autowired) {
                    continue;
                }
                boolean required = autowired.required();
                field.setAccessible(true);
                String name = autowired.name();
                if (!"".equals(name)) {
                    Object bean = beanManager.getBean(name);
                    field.set(target, bean);
                } else {
                    Class<?> fieldType = field.getType();
                    if (fieldType.isArray()) {
                        List<?> beans = beanManager.getBeans(fieldType);
                        Object array = Array.newInstance(fieldType.getComponentType(), beans.size());
                        for(int i = 0; i < beans.size(); i++) {
                            Array.set(array, i, beans.get(i));
                        }
                        field.set(target, array);
                    } else if (fieldType.equals(List.class)) {
                        List<?> beans = beanManager.getBeans(fieldType);
                        field.set(target, beans);
                    } else {
                        Object bean = beanManager.getBean(fieldType);
                        if (null == bean && required) {
                            throw new JspletBeanException("自动装配失败，未发现：" + fieldType.getName());
                        }
                        field.set(target, bean);
                    }
                }
            }
        } catch (IllegalAccessException ex) {
            throw new JspletPageLoadException(ex);
        }
    }

    private Object loadProperty(Element ele) {
        if (ele.hasAttribute("value")) {
            return ele.getAttribute("value");
        }
        if (ele.hasAttribute("ref")) {
            String ref = ele.getAttribute("ref");
            return beanManager.getBean(ref);
        }
        Element node = ElementUtil.getElement(ele, "value");
        if (null != node) {
            return node.getTextContent();
        }
        node = ElementUtil.getElement(ele, "bean");
        if (null != node) {
            BeanElement beanElement = new BeanElement(beanManager, node);
            beanElement.build();
            return beanElement.getObject();
        }
        node = ElementUtil.getElement(element, "list");
        if (null != node) {
            return loadList(node);
        }
        node = ElementUtil.getElement(element, "map");
        if (null != node) {
            return loadMap(node);
        }
        return null;
    }

    protected List<Object> loadList(Element parent) {
        List<Object> list = new ArrayList<>();
        List<Element> nodes = ElementUtil.getElements(parent);
        for (Element node : nodes) {
            String tagName = node.getTagName();
            if ("value".equals(tagName)) {
                String value = node.getTextContent();
                list.add(value);
            } else if ("ref".equals(tagName)) {
                String ref = node.getAttribute("bean");
                Object value = beanManager.getBean(ref);
                list.add(value);
            } else if ("bean".equals(tagName)) {
                BeanElement beanElement = new BeanElement(beanManager, node);
                beanElement.build();
                Object value = beanElement.getObject();
                list.add(value);
            } else if ("list".equals(tagName)) {
                List<Object> value = loadList(node);
                list.add(value);
            } else if ("map".equals(tagName)) {
                Map<String, Object> value = loadMap(node);
                list.add(value);
            }
        }
        return list;
    }

    private Map<String, Object> loadMap(Element parent) {
        Map<String, Object> map = new HashMap<>();
        List<Element> nodes = ElementUtil.getElements(parent, "entry");
        for (Element node : nodes) {
            String key = node.getAttribute("key");
            Object value = loadProperty(node);
            map.put(key, value);
        }
        return map;
    }

    public void init() {
        try {
            Class<?> type = target.getClass();
            if (null != initMethod && !"".equals(initMethod)) {
                Method method = type.getMethod(initMethod);
                method.invoke(target);
            }
            Method[] methods = type.getMethods();
            for (Method method : methods) {
                PostConstruct annotation = method.getAnnotation(PostConstruct.class);
                if (null == annotation) {
                    continue;
                }
                method.invoke(target);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new JspletConfigException(ex);
        }
    }

    public void destroy() {
        try {
            Class<?> type = target.getClass();
            if (null != destroyMethod && !"".equals(destroyMethod)) {
                Method method = type.getMethod(destroyMethod);
                method.invoke(target);
            }
            Method[] methods = type.getMethods();
            for (Method method : methods) {
                PreDestroy annotation = method.getAnnotation(PreDestroy.class);
                if (null == annotation) {
                    continue;
                }
                method.invoke(target);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new JspletException(ex);
        }
    }

    public Class<?> getObjectType() {
        if (target instanceof FactoryBean) {
            return ((FactoryBean<?>) target).getObjectType();
        } else {
            return target.getClass();
        }
    }

    public Object getObject() {
        if (target instanceof FactoryBean) {
            FactoryBean<?> factory = (FactoryBean<?>) target;
            if (factory.isSingleton()) {
                if (null == instance) {
                    synchronized (this) {
                        if (null == instance) {
                            instance = factory.getObject();
                        }
                    }
                }
                return instance;
            } else {
                return factory.getObject();
            }
        } else {
            return target;
        }
    }

}
