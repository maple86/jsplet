package com.maple86.jsplet.core;

import com.maple86.jsplet.annotation.*;
import com.maple86.jsplet.api.ActionExecutor;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.api.JspletModule;
import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.CloseSessionInvoker;
import com.maple86.jsplet.exception.JspletStatusException;
import com.maple86.jsplet.interceptor.InterceptorHandler;
import com.maple86.jsplet.interceptor.InterceptorInvocation;
import com.maple86.jsplet.reflect.MethodInvoker;
import com.maple86.jsplet.result.ErrorStatus;
import com.maple86.jsplet.result.ModelMap;
import com.maple86.jsplet.util.BeanUtil;
import com.maple86.jsplet.weaver.ActionWeaverInvoker;
import com.maple86.jsplet.weaver.WeaverInvoker;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

public class ActionExecutorImpl implements ActionExecutor {

    private final Log log = LogFactory.getLog(this.getClass());

    private final JspletPage jspletPage;

    public ActionExecutorImpl(JspletPage jspletPage) {
        this.jspletPage = jspletPage;
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response) throws Throwable {
        JspletModule jspletModule = jspletPage.getJspletModule();
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        List<InterceptorHandler> interceptorHandlers = jspletEngine.getBeans(InterceptorHandler.class);
        InterceptorInvocation invocation = new InterceptorInvocation(this::handle, null);
        for(int i = interceptorHandlers.size() - 1; i >= 0; i--) {
            InterceptorHandler interceptorHandler = interceptorHandlers.get(i);
            invocation = new InterceptorInvocation(interceptorHandler, invocation);
        }
        return invocation.invoke(request, response);
    }

    private Object handle(HttpServletRequest request, HttpServletResponse response, InterceptorInvocation invocation) throws Throwable {
        String methodName = request.getMethod();
        try {
            Object target = jspletPage.getTarget();
            Method method = getPageMethod(target, methodName);
            if (null == method) {
                return new ErrorStatus(403, "没有定义方法：" + methodName);
            }
            log.trace("开始执行，页面：" + jspletPage.getPagePath() + "，方法：" + method.getName());
            JspletModule jspletModule = jspletPage.getJspletModule();
            JspletEngine jspletEngine = jspletModule.getJspletEngine();
            Object[] arguments = obtainArguments(method, request, response);
            if (log.isDebugEnabled()) {
                log.trace("执行参数：" + Arrays.toString(arguments));
            }
            WeaverInvoker weaverInvoker = new ActionWeaverInvoker(jspletPage, request, response, this::invoke);
            MethodInvoker methodInvoker = new CloseSessionInvoker(jspletEngine, weaverInvoker);
            Object result = methodInvoker.invoke(target, method, arguments);
            log.trace("结束执行，页面：" + jspletPage.getPagePath() + "，方法：" + method.getName());
            if (log.isDebugEnabled()) {
                log.trace("执行结果：" + result);
            }
            return result;
        } catch (JspletStatusException ex) {
            return new ErrorStatus(ex.getStatus(), ex.getMessage());
        }
    }

    private Method getPageMethod(Object target, String name) {
        Class<? extends Annotation> annotationClass = getAnnotationClass(name);
        if (null == annotationClass) {
            return null;
        }
        Method[] methods = target.getClass().getMethods();
        for (Method method : methods) {
            Annotation annotation = method.getAnnotation(annotationClass);
            if (null != annotation) {
                return method;
            }
        }
        return null;
    }

    private Class<? extends Annotation> getAnnotationClass(String name) {
        switch(name) {
            case "GET":
                return GetMethod.class;
            case "POST":
                return PostMethod.class;
            case "DELETE":
                return DeleteMethod.class;
            case "PUT":
                return PutMethod.class;
            case "OPTIONS":
                return OptionsMethod.class;
            case "HEAD":
                return HeadMethod.class;
            case "TRACE":
                return TraceMethod.class;
            case "PATCH":
                return PatchMethod.class;
            default:
                return null;
        }
    }

    private Object invoke(Object target, Method method, Object[] arguments) throws Throwable {
        try {
            return jspletPage.invoke(method, arguments);
        } catch (Throwable throwable) {
            log.error("", throwable);
            throw throwable;
        }
    }

    private Object[] obtainArguments(Method method, HttpServletRequest request, HttpServletResponse response) throws IllegalAccessException, InstantiationException, FileUploadException, UnsupportedEncodingException {
        Map<String, List<Object>> parametersMap = getParametersMap(request);
        Parameter[] parameters = method.getParameters();
        Object[] args = new Object[parameters.length];
        for(int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Class<?> type = parameter.getType();
            if (type.isInstance(request)) {
                args[i] = request;
                continue;
            }
            if (type.isInstance(response)) {
                args[i] = response;
                continue;
            }
            if (ServletContext.class.equals(type)) {
                args[i] = request.getServletContext();
                continue;
            }
            if (HttpSession.class.equals(type)) {
                args[i] = request.getSession();
                continue;
            }
            if (ModelMap.class.equals(type)) {
                args[i] = new ModelMap();
                continue;
            }
            Param param = parameter.getAnnotation(Param.class);
            if (null != param) {
                String name = param.value();
                if ("".equals(name)) {
                    name = param.field();
                }
                args[i] = getParameter(parametersMap, name, type);
                if (param.required() && null == args[i]) {
                    throw new JspletStatusException(403, "缺少必要参数：" + name);
                }
                continue;
            }
            ParamModel paramModel = parameter.getAnnotation(ParamModel.class);
            if (null != paramModel) {
                String modelName = paramModel.value();
                Object argument = type.newInstance();
                Field[] fields = type.getDeclaredFields();
                for (Field field : fields) {
                    param = field.getAnnotation(Param.class);
                    if (null == param) {
                        continue;
                    }
                    String name = param.field();
                    if ("".equals(name)) {
                        name = param.value();
                    }
                    if ("".equals(name)) {
                        name = field.getName();
                    }
                    if (!"".equals(modelName)) {
                        name = modelName + "." + name;
                    }
                    Object value = getParameter(parametersMap, name, field.getType());
                    if (param.required() && null == value) {
                        throw new JspletStatusException(403, "缺少必要参数：" + name);
                    }
                    if (null != value) {
                        BeanUtil.setProperty(argument, field.getName(), value);
                    }
                }
                args[i] = argument;
            }
        }
        return args;
    }

    private Object getParameter(Map<String, List<Object>> parametersMap, String name, Class<?> type) {
        List<Object> list = parametersMap.get(name);
        if (null == list || list.isEmpty()) {
            return null;
        }
        Object result;
        if (type.isArray()) {
            result = BeanUtil.parseType(list, type);
        } else {
            result = BeanUtil.parseType(list.get(0), type);
        }
        if (null == result && !"".equals(list.get(0))) {
            throw new JspletStatusException(400, "请求参数类型错误：" + name);
        }
        return result;
    }

    private Map<String, List<Object>> getParametersMap(HttpServletRequest request) throws FileUploadException, UnsupportedEncodingException {
        Map<String, List<Object>> parametersMap = new HashMap<>();
        JspletModule jspletModule = jspletPage.getJspletModule();
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        FileItemFactory fileItemFactory = jspletEngine.getFileItemFactory();
        if (ServletFileUpload.isMultipartContent(request)) {
            ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
            long uploadFileMaxSize = jspletSetting.getUploadFileMaxSize();
            long uploadRequestMaxSize = jspletSetting.getUploadRequestMaxSize();
            fileUpload.setFileSizeMax(uploadFileMaxSize);
            fileUpload.setSizeMax(uploadRequestMaxSize);
            List<FileItem> fileItems = fileUpload.parseRequest(request);
            for (FileItem fileItem : fileItems) {
                String name = fileItem.getFieldName();
                List<Object> list = parametersMap.computeIfAbsent(name, key -> new ArrayList<>());
                if (fileItem.isFormField()) {
                    String value = fileItem.getString("UTF-8");
                    list.add(value);
                } else {
                    list.add(fileItem);
                }
            }
        } else {
            Enumeration<String> names = request.getParameterNames();
            while(names.hasMoreElements()) {
                String name = names.nextElement();
                String[] values = request.getParameterValues(name);
                for (String value : values) {
                    List<Object> list = parametersMap.computeIfAbsent(name, key -> new ArrayList<>());
                    list.add(value);
                }
            }
        }
        return parametersMap;
    }
}
