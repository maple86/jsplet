package com.maple86.jsplet.core;

import com.maple86.jsplet.api.BeanFactory;
import com.maple86.jsplet.api.JspletEngine;
import com.maple86.jsplet.exception.JspletBeanException;
import com.maple86.jsplet.exception.JspletConfigException;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;

public class BeanManager {

    private final List<BeanElement> beanElements = new ArrayList<>();

    private final JspletEngine jspletEngine;

    public BeanManager(JspletEngine jspletEngine) {
        this.jspletEngine = jspletEngine;
    }

    public JspletEngine getJspletEngine() {
        return jspletEngine;
    }

    public void register(Class<?> clazz) {
        register(null, clazz);
    }

    public void register(String name, Class<?> clazz) {
        if (null != name && !"".equals(name)) {
            List<String> list = getBeanNames();
            if (list.contains(name)) {
                throw new JspletConfigException("bean名称重复：" + name);
            }
        }
        try {
            Object target = clazz.newInstance();
            BeanElement beanElement = new BeanElement(this, name, target);
            beanElements.add(beanElement);
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new JspletConfigException("", ex);
        }
    }

    public void register(Element element) {
        BeanElement beanElement = new BeanElement(this, element);
        String name = beanElement.getName();
        List<String> list = getBeanNames();
        if (list.contains(name)) {
            throw new JspletConfigException("bean名称重复：" + name);
        }
        beanElements.add(beanElement);
    }

    public void build() {
        beanElements.forEach(BeanElement::build);
    }

    public void init() {
        beanElements.forEach(BeanElement::init);
    }

    public void destroy() {
        beanElements.forEach(BeanElement::destroy);
    }

    public List<String> getBeanNames() {
        List<String> list = new ArrayList<>();
        for (BeanElement beanElement : beanElements) {
            String name = beanElement.getName();
            if (null == name || "".equals(name)) {
                continue;
            }
            list.add(name);
        }
        return list;
    }

    public Object getBean(String name) {
        for (BeanElement beanElement : beanElements) {
            if (name.equals(beanElement.getName())) {
                return beanElement.getObject();
            }
        }
        return null;
    }

    public <T> T getBean(Class<T> type) {
        List<T> beans = getBeans(type);
        if (beans.isEmpty()) {
            return null;
        }
        if (beans.size() > 1) {
            throw new JspletBeanException("存在多个Bean： " + type.getName());
        }
        return beans.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> getBeans(Class<T> type) {
        List<T> list = new ArrayList<>();
        for (BeanElement beanElement : beanElements) {
            Class<?> objectType = beanElement.getObjectType();
            if (type.isAssignableFrom(objectType)) {
                Object bean = beanElement.getObject();
                list.add((T) bean);
            }
            Object target = beanElement.getTarget();
            if (target instanceof BeanFactory) {
                List<T> beans = ((BeanFactory) target).getBeans(type);
                if (null != beans && !beans.isEmpty()) {
                    list.addAll(beans);
                }
            }
        }
        return list;
    }

}
