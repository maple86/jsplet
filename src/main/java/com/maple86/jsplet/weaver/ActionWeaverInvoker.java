package com.maple86.jsplet.weaver;

import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.reflect.MethodInvoker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

public class ActionWeaverInvoker extends WeaverInvoker {

    private JspletPage jspletPage;

    private HttpServletRequest request;

    private HttpServletResponse response;

    public ActionWeaverInvoker(JspletPage jspletPage, HttpServletRequest request, HttpServletResponse response, MethodInvoker methodInvoker) {
        super(methodInvoker);
        this.jspletPage = jspletPage;
        this.request = request;
        this.response = response;
    }

    @Override
    protected WeaverContext createWeaverContext(Annotation annotation) {
        return new ActionWeaverContext(jspletPage, annotation, request, response);
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

}
