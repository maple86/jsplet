package com.maple86.jsplet.weaver;

import com.maple86.jsplet.api.JspletPage;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ComponentWeaverContext extends WeaverContext {

    private Map<String, Object> parameterMap;

    public ComponentWeaverContext(JspletPage jspletPage, Annotation annotation, Map<String, Object> parameterMap) {
        super(jspletPage, annotation);
        this.parameterMap = parameterMap;
    }

    public List<String> getParameterNames() {
        return new ArrayList<>(parameterMap.keySet());
    }

    public Object getParameter(String name) {
        return parameterMap.get(name);
    }

}
