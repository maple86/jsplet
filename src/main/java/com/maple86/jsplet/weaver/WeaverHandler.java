package com.maple86.jsplet.weaver;

import com.maple86.jsplet.reflect.MethodInvocation;

public interface WeaverHandler {

    Object handle(WeaverContext weaverContext, MethodInvocation weaverInvocation) throws Throwable;

}
