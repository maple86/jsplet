package com.maple86.jsplet.weaver;

import com.maple86.jsplet.annotation.Weaver;
import com.maple86.jsplet.reflect.MethodInvoker;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public abstract class WeaverInvoker implements MethodInvoker {

    private MethodInvoker methodInvoker;

    public WeaverInvoker(MethodInvoker methodInvoker) {
        this.methodInvoker = methodInvoker;
    }

    @Override
    public Object invoke(Object target, Method method, Object[] arguments) throws Throwable {
        Annotation[] annotations = method.getAnnotations();
        MethodInvoker invoker = this.methodInvoker;
        for(int i = annotations.length - 1; i >= 0; i--) {
            Annotation annotation = annotations[i];
            WeaverHandler handler = getWeaverHandler(annotation.annotationType());
            if (null != handler) {
                WeaverContext weaverContext = createWeaverContext(annotation);
                invoker = new WeaverChain(handler, weaverContext, invoker);
            }
        }
        return invoker.invoke(target, method, arguments);
    }

    protected abstract WeaverContext createWeaverContext(Annotation annotation);

    private WeaverHandler getWeaverHandler(Class<? extends Annotation> type) throws IllegalAccessException, InstantiationException {
        Weaver weaver = type.getAnnotation(Weaver.class);
        if (null == weaver) {
            return null;
        }
        Class<? extends WeaverHandler> clazz = weaver.value();
        return clazz.newInstance();
    }
}
