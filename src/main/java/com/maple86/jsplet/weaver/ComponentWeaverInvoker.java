package com.maple86.jsplet.weaver;

import com.maple86.jsplet.api.JspletPage;
import com.maple86.jsplet.reflect.MethodInvoker;

import java.lang.annotation.Annotation;
import java.util.Map;

public class ComponentWeaverInvoker extends WeaverInvoker {

    private final JspletPage jspletPage;

    public ComponentWeaverInvoker(JspletPage jspletPage, MethodInvoker methodInvoker) {
        super(methodInvoker);
        this.jspletPage = jspletPage;
    }

    @Override
    protected WeaverContext createWeaverContext(Annotation annotation) {
        return new WeaverContext(jspletPage, annotation);
    }
}
