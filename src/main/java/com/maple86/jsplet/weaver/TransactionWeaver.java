package com.maple86.jsplet.weaver;


import com.maple86.jsplet.annotation.Transaction;
import com.maple86.jsplet.api.*;
import com.maple86.jsplet.config.JspletSetting;
import com.maple86.jsplet.db.TransactionLevel;
import com.maple86.jsplet.reflect.MethodInvocation;

public class TransactionWeaver implements WeaverHandler {

    @Override
    public Object handle(WeaverContext weaverContext, MethodInvocation methodInvocation) throws Throwable {
        JspletPage jspletPage = weaverContext.getJspletPage();
        JspletModule jspletModule = jspletPage.getJspletModule();
        Transaction jspletTransaction = (Transaction) weaverContext.getAnnotation();
        TransactionLevel transactionLevel = getTransactionLevel(weaverContext, jspletTransaction);
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        DataSessionFactory dataSessionFactory = jspletEngine.getDataSessionFactory();
        DataSession session = dataSessionFactory.openSession();
        if (session.hasTransaction()) {
            // 如果已经开启了事务，则不处理事务，交由外层处理
            return methodInvocation.invoke();
        } else {
            // 如果没有开启事务，则开启并处理事务
            session.openTransaction(transactionLevel);
            try {
                Object result = methodInvocation.invoke();
                session.commit();
                return result;
            } catch (Exception ex) {
                session.rollback();
                throw ex;
            }
        }
    }

    private TransactionLevel getTransactionLevel(WeaverContext weaverContext, Transaction jspletTransaction) {
        TransactionLevel transactionLevel = jspletTransaction.level();
        if (!TransactionLevel.TRANSACTION_DEFAULT.equals(transactionLevel)) {
            return transactionLevel;
        }
        JspletPage jspletPage = weaverContext.getJspletPage();
        JspletModule jspletModule = jspletPage.getJspletModule();
        JspletEngine jspletEngine = jspletModule.getJspletEngine();
        JspletSetting jspletSetting = jspletEngine.getJspletSetting();
        return jspletSetting.getDefaultTransactionLevel();
    }

}
