package com.maple86.jsplet.weaver;

import com.maple86.jsplet.reflect.MethodInvocation;
import com.maple86.jsplet.reflect.MethodInvoker;

import java.lang.reflect.Method;

public class WeaverChain implements MethodInvoker {

    private WeaverHandler weaverHandler;

    private WeaverContext weaverContext;

    private MethodInvoker methodInvoker;

    public WeaverChain(WeaverHandler weaverHandler, WeaverContext weaverContext, MethodInvoker methodInvoker) {
        this.weaverHandler = weaverHandler;
        this.weaverContext = weaverContext;
        this.methodInvoker = methodInvoker;
    }

    @Override
    public Object invoke(Object target, Method method, Object[] arguments) throws Throwable {
        MethodInvocation weaverInvocation = new MethodInvocation(methodInvoker, target, method, arguments);
        return weaverHandler.handle(weaverContext, weaverInvocation);
    }
}
