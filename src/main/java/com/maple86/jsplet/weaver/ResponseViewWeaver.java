package com.maple86.jsplet.weaver;

import com.maple86.jsplet.exception.JspletException;
import com.maple86.jsplet.reflect.MethodInvocation;
import com.maple86.jsplet.result.ModelMap;
import com.maple86.jsplet.result.ModelView;

public class ResponseViewWeaver implements WeaverHandler {

    @Override
    public Object handle(WeaverContext weaverContext, MethodInvocation methodInvocation) throws Throwable {
        Object[] arguments = methodInvocation.getArguments();
        Object result = methodInvocation.invoke();
        if (result instanceof ModelView) {
            return result;
        }
        if (!(result instanceof String)) {
            throw new JspletException("结果类型错误！");
        }
        String view = (String) result;
        ModelMap model = null;
        for (Object argument : arguments) {
            if (argument instanceof ModelMap) {
                model = (ModelMap) argument;
                break;
            }
        }
        if (null == model) {
            model = new ModelMap();
        }
        return new ModelView(view, model);
    }
}
