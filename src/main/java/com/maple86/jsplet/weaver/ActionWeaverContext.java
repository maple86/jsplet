package com.maple86.jsplet.weaver;

import com.maple86.jsplet.api.JspletPage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

public class ActionWeaverContext extends WeaverContext {

    private HttpServletRequest request;

    private HttpServletResponse response;

    public ActionWeaverContext(JspletPage jspletPage, Annotation annotation, HttpServletRequest request, HttpServletResponse response) {
        super(jspletPage, annotation);
        this.request = request;
        this.response = response;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

}
