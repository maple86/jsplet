package com.maple86.jsplet.weaver;

import com.maple86.jsplet.api.JspletPage;

import java.lang.annotation.Annotation;

public class WeaverContext {

    private JspletPage jspletPage;

    private Annotation annotation;

    public WeaverContext(JspletPage jspletPage, Annotation annotation) {
        this.jspletPage = jspletPage;
        this.annotation = annotation;
    }

    public JspletPage getJspletPage() {
        return jspletPage;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

}
